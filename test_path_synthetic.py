import torch
from scene_loaders.ibr_scene import Scene
from scene_loaders.cameras import CameraSimple
import random
import os
import numpy as np
import math
from tqdm import tqdm
from os import makedirs
from renderer.render_point_cloud import render_scene, render_scene_diffuse_only
import torchvision
from arguments.parse_args import get_args
from scene_loaders.read_scenes_types import readBunderFolder
from utils.general_utils import PILtoTorch
from utils.camera_utils import get_test_cameras, exportListCamsToColmap
from scipy.spatial.transform import Rotation as R
from scipy.spatial.transform import Slerp
from scene_loaders.cameras import Camera

def interpolate_cameras(cam_list, start, stop, total):

    def findclosest_idx(cam, other_cams):
        positions = [x.camera_center for x in other_cams]
        distance = torch.linalg.norm(cam.camera_center - torch.cat(positions, dim=0), dim=1)
        return torch.argmin(distance).item()

    smooth_camlist = []
    nextcam = None

    inter_frames = int((total-1)/(stop-start))
    cropped_list = cam_list[start:stop]
    while len(cropped_list)>0:
        if nextcam is None:
            currcam = cropped_list.pop(0)
        else:
            currcam = nextcam
        nextcam = cropped_list.pop(0)
        keyrots = R.from_matrix([currcam.R, nextcam.R])
        key_times = [0, 1]
        slerp = Slerp(key_times, keyrots)
        for j in range(inter_frames):
            torch.cuda.empty_cache()
            frac = float(j) / float(inter_frames)
            trans = (1. - frac) * currcam.T + frac * nextcam.T
            times = [frac]

            interprot = slerp(times)
            rot = interprot.as_matrix()
            rot = np.moveaxis(rot, (0, 1, 2), (2, 0, 1))

            test_viewpoint = Camera(colmap_id=currcam.uid, R=rot, T=trans, FoVx=currcam.FoVx,
                                    FoVy=currcam.FoVy, gt_alpha_mask=currcam.gt_alpha_mask,
                                    polytope_mask=currcam.polytope_mask,
                                    image=currcam.original_image,
                                    image_name=currcam.image_name,
                                    max_radius=currcam.max_radius,
                                    uid=currcam.uid)
            smooth_camlist.append(test_viewpoint)
    return smooth_camlist

args = get_args()

with torch.no_grad():
    device = torch.device("cuda:0")
    torch.cuda.set_device(device)
    torch.manual_seed(0)
    random.seed(0)

    args.global_downscale = 1000

    scene = Scene(args, resolution_scales=[1.0], shuffle=False)

    # COMPOST
    if "compost" in args.scene_representation_folder:
        cameras_path = interpolate_cameras([scene.getTrainCameras()[0],
                                            scene.getTrainCameras()[45],
                                            scene.getTrainCameras()[101],
                                            scene.getTrainCameras()[267]], start=0, stop=4, total=450)
    if "hallway_lamp" in args.scene_representation_folder:
        cameras_path = interpolate_cameras([scene.getTrainCameras()[138],
                                            scene.getTrainCameras()[165],
                                            scene.getTrainCameras()[59],
                                            scene.getTrainCameras()[42]], start=0, stop=4, total=450)
    if "concave_bowl2" in args.scene_representation_folder:
        cameras_path = interpolate_cameras([scene.getTrainCameras()[7],
                                            scene.getTrainCameras()[24],
                                            scene.getTrainCameras()[199],
                                            scene.getTrainCameras()[69]], start=0, stop=4, total=450)
    if "silver_vase2" in args.scene_representation_folder.split(os.sep)[-2]:
        cameras_path = interpolate_cameras([scene.getTrainCameras()[27],
                                            scene.getTrainCameras()[108],
                                            scene.getTrainCameras()[117],
                                            scene.getTrainCameras()[151]], start=0, stop=4, total=450)
    if "crazy_blade2" in args.scene_representation_folder:
        cameras_path = interpolate_cameras([scene.getTrainCameras()[9],
                                            scene.getTrainCameras()[76],
                                            scene.getTrainCameras()[150],
                                            scene.getTrainCameras()[183]], start=0, stop=4, total=450)

    out_name = "test_path_synthetic_renders"
    debug_path = os.path.join(args.scene_representation_folder, out_name, "iter_{}".format(scene.load_iter), "debug")

    render_path = os.path.join(args.scene_representation_folder, out_name, "iter_{}".format(scene.load_iter), "renders")
    gts_path = os.path.join(args.scene_representation_folder, out_name, "iter_{}".format(scene.load_iter), "gt")
    polytope_mask_path = os.path.join(args.scene_representation_folder, out_name, "iter_{}".format(scene.load_iter), "polytope_masks")

    # makedirs(debug_path, exist_ok=True)
    makedirs(render_path, exist_ok=True)
    makedirs(gts_path, exist_ok=True)
    makedirs(polytope_mask_path, exist_ok=True)

    staticCam = cameras_path[10]

    imgCount = len(cameras_path)

    exportListCamsToColmap(cameras_path,  os.path.join(args.scene_representation_folder, out_name, out_name + "_colmap"))
    for idx, cam in tqdm(enumerate(cameras_path)):
        torch.cuda.empty_cache()
        print("\rExporting images %i/%i" % (idx+1, imgCount), end="")
        gt = torch.cat((cam.original_image[:, 0:3, :, :].cuda(),
                        cam.gt_alpha_mask.cuda()), dim=1)
        polytope_mask = cam.polytope_mask.cuda()

        if not args.diffuse_only:
            view, diffuse_render, specular_render, mask, specular_environment_mask, _, _, _ = render_scene(cam, scene, cata_camera=None)
        else:
            view, diffuse_render, _ = render_scene_diffuse_only(cam, scene)
        if False:
            viewStatic, _, _, _, _, _ = render_scene(staticCam, scene, cata_camera=cam)
        
            collage = torch.cat((gt, view, viewStatic,
                                 diffuse_render[:,:3,...],
                                 specular_render[:,:3,...],
                                 mask.repeat(1,3,1,1)), dim=0)
            torchvision.utils.save_image(torchvision.utils.make_grid(collage, nrow=3), os.path.join(debug_path, "path_" + str(idx).zfill(4) + ".png"))
        torchvision.utils.save_image(view, os.path.join(render_path, "path_" + str(idx).zfill(4) + ".png"))
        torchvision.utils.save_image(gt, os.path.join(gts_path, "path_" + str(idx).zfill(4) + ".png"))
        torchvision.utils.save_image(polytope_mask, os.path.join(polytope_mask_path, "path_" + str(idx).zfill(4) + ".png"))

        #torchvision.utils.save_image(view, os.path.join(outpath, "path_{}.png".format(idx)))
        # torchvision.utils.save_image(diffuse_render[:,:3,...], os.path.join(outpath, "diffuse_{}.png".format(idx)))
        #torchvision.utils.save_image(specular_render[:,:3,...], os.path.join(outpath, "specular_{}.png".format(idx)))
        # torchvision.utils.save_image(mask, os.path.join(outpath, "mask_{}.png".format(idx)))

        # torchvision.utils.save_image(images[idx], os.path.join(outpath, "gt_{}.png".format(idx)))