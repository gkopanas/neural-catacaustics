import numpy as np
from scipy.spatial import ConvexHull, HalfspaceIntersection
from scipy.optimize import linprog
from simplification.cutil import simplify_coords
import open3d as o3d
from utils.graphics_utils import getDirRays3
import matplotlib.pyplot as plt
# ============================================================

def createPolytope(planePoints):
    # ---------------------------------------

    # given 3 points, return plane equation of the form ax+by+cz+d=0
    def planeFromPoints(x1, y1, z1, x2, y2, z2, x3, y3, z3):
        a1, b1, c1 = x2 - x1, y2 - y1, z2 - z1
        a2, b2, c2 = x3 - x1, y3 - y1, z3 - z1
        a = b1 * c2 - b2 * c1
        b = a2 * c1 - a1 * c2
        c = a1 * b2 - b1 * a2
        d = - a * x1 - b * y1 - c * z1
        return -np.array([a, b, c, d])

    # ---------------------------------------

    # given a list of edges in the form of index tuples,
    # return a sorted list such that the edges build a closed chain
    def sortFaceEdges(f):
        sortedFaces = []
        idx = 0
        while f:
            sortedFaces.append(f[idx])
            link = f[idx][1]
            del f[idx]
            for si, s in enumerate(f):
                if link in s:
                    if s[1] == link:
                        f[si] = [f[si][1], f[si][0]]
                    idx = si
        assert sortedFaces[0][0] == sortedFaces[-1][1], "Need closed loops."
        return sortedFaces

    # ---------------------------------------

    # use linear programming to find a point inside the polytope
    def findInteriorPoint(halfSpaces):
        norm_vector = np.reshape(np.linalg.norm(halfSpaces[:, :-1], axis=1), (halfSpaces.shape[0], 1))
        c = np.zeros((halfSpaces.shape[1],))
        c[-1] = -1
        A = np.hstack((halfSpaces[:, :-1], norm_vector))
        b = -halfSpaces[:, -1:]
        res = linprog(c, A_ub=A, b_ub=b, bounds=(None, None))
        return res.x[:-1]

    # ---------------------------------------

    planeCount = planePoints.shape[0]

    halfSpaces = np.zeros((planeCount, 4))

    for idx in range(planeCount):
        halfSpaces[idx] = planeFromPoints(*planePoints[idx, 0], *planePoints[idx, 1], *planePoints[idx, 2])

    print("Obtaining interior point...")
    interiorPoint = findInteriorPoint(halfSpaces)

    print("Solving for dual polytope...")
    hsi = HalfspaceIntersection(halfSpaces, interiorPoint, qhull_options='Qt')

    dualPolytope = o3d.geometry.TriangleMesh()
    dualPolytope.vertices = o3d.utility.Vector3dVector(hsi.dual_points)
    dualPolytope.triangles = o3d.utility.Vector3iVector(hsi.dual_facets)
    dualPolytope.compute_adjacency_list()

    # -------------------------------------------
    # convert from dual to primal polytope;
    # we already have the vertices, but now we need to collect the faces

    print("Converting to primal polytope...")

    polytope = o3d.geometry.TriangleMesh()
    polytope.vertices = o3d.utility.Vector3dVector(hsi.intersections)
    polytope.paint_uniform_color([0.9, 0.1, 0.1])

    primalFaces = []
    for idx, al in enumerate(dualPolytope.adjacency_list):
        primalEdges = []
        for neighbor in al:
            adjacentFaces = []
            for idx_f, f in enumerate(hsi.dual_facets):
                if all(x in f for x in [idx, neighbor]):
                    adjacentFaces.append(idx_f)
            primalEdges.append(adjacentFaces)
        if primalEdges:
            primalFaces.append(primalEdges)

    # -------------------------------------------
    # manually triangulate polytope faces

    primalTris = []

    for f in primalFaces:
        sortedFaces = sortFaceEdges(f)
        baseIdx = sortedFaces[0][0]
        for idx in range(len(sortedFaces) - 2):
            tri = [baseIdx, sortedFaces[idx + 1][0], sortedFaces[idx + 2][0]]

            # take care of winding order
            p1, p2, p3 = hsi.intersections[tri[0]], hsi.intersections[tri[1]], hsi.intersections[tri[2]]
            a, b, c, d = planeFromPoints(*p1, *p2, *p3)
            sign = a * interiorPoint[0] + b * interiorPoint[1] + c * interiorPoint[2] + d
            if sign < 0:
                tri = [tri[0], tri[2], tri[1]]

            primalTris.append(tri)

    polytope.triangles = o3d.utility.Vector3iVector(primalTris)

    return polytope


def create_scene_polytope(cameras, reflection_estimation_masks, debug_path=None):
    simplification_weight = 5
    triSize = 2.5
    
    meshes = []
    vizMeshes = []
    center = o3d.geometry.TriangleMesh.create_coordinate_frame(size=0.2)
    vizMeshes.append(center)

    for camIdx, camera in enumerate(cameras):
        if reflection_estimation_masks[camera.image_name] is None:
            continue
        mask = reflection_estimation_masks[camera.image_name].cpu().squeeze().numpy()

        print("Processing cam %i" % camIdx)

        res = mask.shape

        # --------------------------------------------------
        # get 2D polyline
        print("\tCreating 2D polyline...")

        # extract non-black pixel locations
        x, y = np.arange(0, res[1], 1), np.arange(0, res[0], 1)
        xx, yy = np.meshgrid(x, y)
        coords2D = np.stack([xx, yy], axis=-1)[mask > 0.0]

        # convex hull
        hull = ConvexHull(coords2D)
        hullVertices = np.array([coords2D[hull.vertices, 0], coords2D[hull.vertices, 1]])
        hullVertices = np.ascontiguousarray(hullVertices.T.astype(np.float32))

        # simplification
        simplifiedHullVertices = simplify_coords(hullVertices, simplification_weight).astype(int)
        vertexCount2D = simplifiedHullVertices.shape[0]

        print("\t\t# 2D Vertices (before/after simplification): %i/%i" % (hullVertices.shape[0], vertexCount2D))

        # visualize results
        if debug_path:
            fig, ax = plt.subplots(figsize=(10, res[0]/res[1]*10))
            ax.imshow(mask, cmap=plt.cm.gray)
            #ax.plot(hullVertices[:,0], hullVertices[:,1], 'ro-', linewidth=1)
            ax.fill(simplifiedHullVertices[:,0], simplifiedHullVertices[:,1], edgecolor='g', linewidth=2, fill=False)
            ax.plot(simplifiedHullVertices[:,0], simplifiedHullVertices[:,1], 'go')
            plt.tight_layout()
            plt.savefig(debug_path + "/viz_" + str(camIdx).zfill(4) + ".pdf")


        # --------------------------------------------------
        # create 3D planes
        print("\tLifting to 3D...")

        allRays = -getDirRays3(camera).cpu().numpy()[0]
        rays = np.zeros((vertexCount2D, 3), dtype=np.float32)
        for idx, v in enumerate(simplifiedHullVertices):
            rays[idx, :] = allRays[:, v[1], v[0]]

        # create 3D triangles from adjacent camera rays
        orig = camera.camera_center.cpu().numpy()

        vertices3D = np.zeros((vertexCount2D + 1, 3), dtype=np.float32)
        triangles = np.zeros((vertexCount2D, 3), dtype=np.int32)
        vertices3D[0] = orig

        for idx in range(vertexCount2D):
            vertices3D[idx + 1] = orig + triSize * rays[idx]
            triIdx1 = idx + 1
            triIdx2 = idx + 2 if idx < vertexCount2D - 1 else 1
            triangles[idx] = [0, triIdx1, triIdx2]

        mesh = o3d.geometry.TriangleMesh()
        mesh.vertices = o3d.utility.Vector3dVector(vertices3D)
        mesh.triangles = o3d.utility.Vector3iVector(triangles)
        meshes.append(mesh)

        camSphere = o3d.geometry.TriangleMesh.create_sphere(radius=0.05).translate(*orig)
        vizMeshes.append(camSphere)

    # --------------------------------------------------
    # expand point representation

    planePoints = []
    for idx, m in enumerate(meshes):
        for t in m.triangles:
            p = np.array((m.vertices[t[0]], m.vertices[t[1]], m.vertices[t[2]]))
            planePoints.append(p)
    planePoints = np.array(planePoints)

    # --------------------------------------------------
    # create polytope

    polytope = createPolytope(planePoints)

    return polytope, vizMeshes, meshes


def render_mask(camera, polytope):
    material = o3d.visualization.rendering.Material()

    res = tuple(camera.original_image.shape[2:4])

    render = o3d.visualization.rendering.OffscreenRenderer(*res)
    render.scene.add_geometry("polytope", polytope, material)

    cam_center = camera.camera_center.cpu().numpy()[0]
    up, _, lookat = camera.getOrientation()
    render.scene.camera.look_at(cam_center + lookat, cam_center, up)
    render.scene.camera.set_projection(
        np.rad2deg(camera.FoVx),
        res[0] / res[1],
        0.001, 100,
        o3d.visualization.rendering.Camera.FovType.Vertical)

    return render.render_to_depth_image()