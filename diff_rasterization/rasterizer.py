from typing import NamedTuple
import torch.nn as nn
import torch

from utils.graphics_utils import getNormalisedImageGrid, getImageGrid
from .rasterize_points import rasterize_points

class PointsRasterizationSettings(NamedTuple):
    image_height: int = 256
    image_width: int =256
    zfar: float = None
    znear: float = None
    gamma: float = 1.0
    layers: int = 1
    fastVersion: bool = False

class PointsRasterizer(nn.Module):
    def __init__(self, raster_settings):
        super().__init__()
        self.raster_settings = raster_settings


    def forward(self, points_screen, features, alpha_values, normals, inv_cov, max_radius):
        raster_settings = self.raster_settings
        idx, color, mask = rasterize_points(
            points_screen,
            features,
            alpha_values,
            inv_cov,
            max_radius,
            image_height=raster_settings.image_height,
            image_width=raster_settings.image_width,
            zfar=raster_settings.zfar,
            znear=raster_settings.znear,
            gamma=raster_settings.gamma,
            fast_version=raster_settings.fastVersion
        )

        #depth_map = points_screen[..., 2][max_w_idx.long()]
        #normal_map = normals[max_w_idx.long()].squeeze(0).permute(0,3,1,2)

        return color, mask, None, None
