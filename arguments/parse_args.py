import argparse
import sys
from argparse import Namespace
import os

def mergeDictsOverwriteEmpty(d1, d2):
    res = d2.copy()
    for k,v in d1.items():
        if v is not None:
            res[k] = v
    return res

def merge_two_dicts(starting_dict: dict, updater_dict: dict) -> dict:
    """
    Starts from base starting dict and then adds the remaining key values from updater replacing the values from
    the first starting/base dict with the second updater dict.

    For later: how does d = {**d1, **d2} replace collision?

    :param starting_dict:
    :param updater_dict:
    :return:
    """
    new_dict: dict = starting_dict.copy()   # start with keys and values of starting_dict
    new_dict = mergeDictsOverwriteEmpty(new_dict, updater_dict)    # modifies starting_dict with keys and values of updater_dict
    return new_dict

def merge_args(args1: Namespace, args2: Namespace) -> Namespace:
    """

    ref: https://stackoverflow.com/questions/56136549/how-can-i-merge-two-argparse-namespaces-in-python-2-x
    :param args1:
    :param args2:
    :return:
    """
    # - the merged args
    # The vars() function returns the __dict__ attribute to values of the given object e.g {field:value}.
    merged_key_values_for_namespace: dict = merge_two_dicts(vars(args1), vars(args2))
    args = Namespace(**merged_key_values_for_namespace)
    return args

def get_args():
    parser = argparse.ArgumentParser(description='Train your network sailor.')
    parser.add_argument('-i', '--input_path', required=False)
    parser.add_argument('-o', '--output_path', required=False, default="")

    parser.add_argument('--load_iter', required=False, type=int, default=None)

    mlp_lr = 0.0005
    cnns_lr = 0.0005
    position_lr = 0.001
    feature_lr = 0.05
    uncertainty_lr = 0.001
    normal_lr = 0.003
    alpha_lr = 0.005

    parser.add_argument("--mlp_lr", required=False, type=float, default=mlp_lr)
    parser.add_argument("--cnns_lr", required=False, type=float, default=cnns_lr)
    parser.add_argument('--neural_lr', required=False, type=float, default=0.0)
    parser.add_argument('--diffuse_xyz_lr', required=False, type=float, default=position_lr)
    parser.add_argument('--specular_xyz_lr', required=False, type=float, default=0.0)
    parser.add_argument('--diffuse_uncertainty_lr', required=False, type=float, default=uncertainty_lr)
    parser.add_argument('--specular_uncertainty_lr', required=False, type=float, default=uncertainty_lr)
    parser.add_argument('--feature_lr', required=False, type=float, default=feature_lr)
    parser.add_argument('--normal_lr', required=False, type=float, default=normal_lr)
    parser.add_argument('--exp_coef_lr', required=False, type=float, default=0.0)
    parser.add_argument('--image_lr', required=False, type=float, default=0.0)
    parser.add_argument('--alpha_lr', required=False, type=float, default=alpha_lr)

    parser.add_argument('--lamda_polytope', required=False, type=float, default=0.01)
    parser.add_argument('--lamda_specular', required=False, type=float, default=0.05)
    parser.add_argument('--lambda_cata', required=False, type=float, default=0.0) #Suggestion: 0.01
    parser.add_argument('--lambda_tv', required=False, type=float, default=0.00001)
    parser.add_argument('--lambda_dssim', required=False, type=float, default=0.2)

    parser.add_argument('--mlp_weight_decay', required=False, type=float, default=1e-5)

    parser.add_argument('--exp_coef_reg_w', required=False, type=float, default=0.0)
    parser.add_argument('--photocons_L_w', required=False, type=float, default=0.0)

    parser.add_argument('--kernel_size', required=False, type=int, default=1)
    parser.add_argument('--residual_blocks', required=False, type=int, default=16)
    parser.add_argument('--internal_depth', required=False, type=int, default=64)
    parser.add_argument('--dropout', required=False, type=float, default=0.0)
    parser.add_argument('--reguralization', required=False, type=float, default=0.0)

    parser.add_argument('--skip_validation', action='store_true', dest='skip_validation')

    parser.add_argument('--extra_features', type=int, default=3)
    parser.add_argument('--max_radius', required=False, type=int, default=8)

    parser.add_argument('--sample_views', action='store_true', dest='sample_views')
    parser.add_argument('--test_cameras', required=False, default=5)

    parser.add_argument('--scene_representation_folder', required=False)

    parser.add_argument('--total_specular_points', required=False, default=int(4 * 10 ** 5))
    parser.add_argument('--total_iterations', required=False, default=int(2*10**6))

    parser.add_argument('--nerfpp_normalize', required=False, default=False)
    parser.add_argument('--detect_anomaly', action='store_true', default=False)
    parser.add_argument('--diffuse_only', action='store_true', default=False)
    parser.add_argument('--no_diffuse_pc_normals', action='store_true', default=False)

    parser.add_argument('--warp_mlp_width', required=False, default=256)
    parser.add_argument('--warp_mlp_depth', required=False, default=4)

    parser.add_argument('--resolution_scale', required=False, default=4.0)
    parser.add_argument('--iter_densify', type=int, required=False, default=5000)
    parser.add_argument('--global_downscale', type=int, required=False, default=1000, help="number of pixels along width for the highest level of resolution.")
    parser.add_argument('--no_mask', action='store_true', default=False)

    parser.add_argument('--experimental_render', action='store_true', default=False)
    parser.add_argument('--se3_warp', action='store_true', default=False)

    parser.add_argument('--polytope_only', action='store_true', default=False)

    parser.add_argument('--no_densify', action='store_true', default=False)

    parser.add_argument('--background', required=False, default="env_map")
    parser.add_argument('--patch_size', required=False, default=160)

    parser.add_argument('--iter_save', required=False, type=int, default=50000, help="number of iterations before saving point clouds and weights")
    parser.add_argument('--iter_validation', required=False, type=int, default=2000, help="number of iterations before computing validation")
    parser.add_argument('--densify_grad_threshold', required=False, type=float, default=0.0002, help="gradient threshold used for gradient-based densification")

    cmdlne_string = sys.argv[1:]
    cfgfile_string = "Namespace()"
    args_cmdline = parser.parse_args(cmdlne_string)

    try:
        cfgfilepath = os.path.join(args_cmdline.scene_representation_folder, "cfg_args")
        with open(cfgfilepath) as cfg_file:
            print("Config File found: {}".format(cfgfilepath))
            cfgfile_string = cfg_file.read()
    except TypeError:
        print("Config file not found")

    args_cfgfile = eval(cfgfile_string)
    args_cmdline = parser.parse_args(cmdlne_string)

    args = merge_args(args_cmdline, args_cfgfile)

    return args
