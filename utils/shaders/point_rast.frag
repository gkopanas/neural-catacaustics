#version 420

layout(location=0) out vec4 out_feats0_l1;
layout(location=1) out vec4 out_feats0_l2;
layout(location=2) out vec4 out_feats0_l3;
layout(location=3) out vec4 out_feats1_l1;
layout(location=4) out vec4 out_feats1_l2;
layout(location=5) out vec4 out_feats1_l3;
layout(location=6) out vec4 out_aux;

layout (binding = 0) uniform sampler2D depth1Texture2D;
layout (binding = 1) uniform sampler2D depth2Texture2D;

in vec4 vFeats0;
in vec3 vFeats1;
in float vAlpha;
in vec3 vInvCov;
in float vPointSize;

void main()
{	
	out_feats0_l1 = vec4(0);
	out_feats0_l2 = vec4(0);
	out_feats0_l3 = vec4(0);
	out_feats1_l1 = vec4(0);
	out_feats1_l2 = vec4(0);
	out_feats1_l3 = vec4(0);
	
	// compute total weight
	vec2 diff = (2 * gl_PointCoord - 1) * vPointSize / 2;
	float exponent = -0.5 * (	vInvCov.x * diff.x * diff.x + 
								2 * vInvCov.y * diff.x * diff.y + 
								vInvCov.z * diff.y * diff.y);
	if (exponent > 0) discard;
	float w = exp(exponent) * vAlpha;
	if (w < 1./255) discard;
	float w_acc = log(1-w);

	// split into 3 layers
	float refDepth1 = texelFetch(depth1Texture2D, ivec2(gl_FragCoord.xy), 0).x;
	float refDepth2 = texelFetch(depth2Texture2D, ivec2(gl_FragCoord.xy), 0).x;

	// 1st layer
	if (gl_FragCoord.z < refDepth1)
	{
		out_feats0_l1 = vFeats0 * w;
		out_feats1_l1 = vec4(vFeats1 * w, w);
		out_aux = vec4(w_acc, 0, 0, 1);
	}
	// 2nd layer
	else if (gl_FragCoord.z < refDepth2)
	{
		out_feats0_l2 = vFeats0 * w;
		out_feats1_l2 = vec4(vFeats1 * w, w);
		out_aux = vec4(0, w_acc, 0, 1);
	}
	// 3rd layer
	else
	{
		out_feats0_l3 = vFeats0 * w;
		out_feats1_l3 = vec4(vFeats1 * w, w);
		out_aux = vec4(0, 0, w_acc, 1);
	}
}