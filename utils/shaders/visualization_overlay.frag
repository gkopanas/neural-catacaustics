#version 420

layout (binding = 0) uniform sampler2D inputTexture2D;
layout (binding = 1) uniform sampler2D viz1Texture2D;
layout (binding = 2) uniform sampler2D viz2Texture2D;

uniform vec2 pointPosition;

in vec2 uv;

out vec4 out_color;

void main()
{	
	out_color = texture(inputTexture2D, uv);
	//out_color = vec4(0);
	
	ivec2 pixCoord = ivec2(gl_FragCoord.xy);

	// selected point overlay
	int radius = 7;
	float dst = distance(pixCoord, pointPosition);
	if (dst < radius) out_color = vec4(1, 0, 0, 1);

	// viz textures overlay
	ivec2 vizRes = textureSize(viz1Texture2D, 0);
	if (pixCoord.x < vizRes.x && pixCoord.y < vizRes.y)
	{
		out_color.rgb = texelFetch(viz1Texture2D, pixCoord, 0).rgb;
	}
	if (pixCoord.x < vizRes.x && pixCoord.y >= vizRes.y && pixCoord.y < 2 * vizRes.y)
	{
		out_color.rgb = texelFetch(viz2Texture2D, pixCoord - ivec2(0, vizRes.y), 0).rgb;
	}

}
