#version 420

layout(binding=0) uniform sampler2D feats0_l1_texture2D;
layout(binding=1) uniform sampler2D feats0_l2_texture2D;
layout(binding=2) uniform sampler2D feats0_l3_texture2D;
layout(binding=3) uniform sampler2D feats1_l1_texture2D;
layout(binding=4) uniform sampler2D feats1_l2_texture2D;
layout(binding=5) uniform sampler2D feats1_l3_texture2D;
layout(binding=6) uniform sampler2D aux_texture2D;

in vec2 uv;

out vec4 out_feats0;
out vec4 out_feats1;

void main()
{	
	vec4 feats0_layer1 = texture(feats0_l1_texture2D, uv);
	vec4 feats0_layer2 = texture(feats0_l2_texture2D, uv);
	vec4 feats0_layer3 = texture(feats0_l3_texture2D, uv);
	vec4 feats1_layer1 = texture(feats1_l1_texture2D, uv);
	vec4 feats1_layer2 = texture(feats1_l2_texture2D, uv);
	vec4 feats1_layer3 = texture(feats1_l3_texture2D, uv);
	vec3 alphas = texture(aux_texture2D, uv).xyz;
	
	alphas = vec3(1) - exp(alphas);

	if (feats1_layer1.a != 0) 
	{
		feats0_layer1 *= alphas.x / feats1_layer1.a;
		feats1_layer1 *= alphas.x / feats1_layer1.a;
	}
	if (feats1_layer2.a != 0) 
	{
		feats0_layer2 *= alphas.y / feats1_layer2.a;
		feats1_layer2 *= alphas.y / feats1_layer2.a;
	}
	if (feats1_layer3.a != 0) 
	{
		feats0_layer3 *= alphas.z / feats1_layer3.a;
		feats1_layer3 *= alphas.z / feats1_layer3.a;
	}

	float final_alpha = (1 - alphas.x) * (1 - alphas.y) * (1 - alphas.z);
	
	out_feats0	   = 	  feats0_layer3 * (1 - alphas.y) * (1 - alphas.x)
						+ feats0_layer2 * (1 - alphas.x) 
						+ feats0_layer1;
	out_feats1.rgb = 	  feats1_layer3.rgb * (1 - alphas.y) * (1 - alphas.x)
						+ feats1_layer2.rgb * (1 - alphas.x) 
						+ feats1_layer1.rgb;
	out_feats1.a = final_alpha;
	
	//out_feats0.rgb = feats0_layer3.rgb;

}
