import math
import numpy as np
import copy
from scipy.spatial.transform import Rotation
from graphics_utils import getPatchProjectionMatrix, fov2focal

#==============================================

# create a 3D translation matrix
def getTranslationMatrix(translation):
    m = np.eye(4)
    m[0:3, 3] = translation
    return m

#==============================================

# create a 3D rotation matrix from three angles
def getRotationMatrix(angles, center=None, order='yxz'):
    m = np.eye(4)
    m[0:3, 0:3] = Rotation.from_euler(order, angles, degrees=True).as_matrix()
    m[0:3, 0:3] = np.transpose(m[0:3, 0:3])
    if center is not None:
        center = np.array(center)
        shift1 = getTranslationMatrix(-center)
        shift2 = getTranslationMatrix(center)
        m = shift2 @ m @ shift1
    return m

#==============================================

# apply a camera transformation to the render camera
def applyCameraTransformations(cam, t=np.eye(4), r=np.eye(4)):
    if not (np.array_equal(t, np.eye(4)) and np.array_equal(r, np.eye(4))):
        oldViewMatrix = cam.world_view_transformCPU[0]
        newViewMatrix = t @ oldViewMatrix @ r
        rot = np.transpose(newViewMatrix[0:3, 0:3])
        trans = newViewMatrix[0:3, 3]
        cam.updateCamera(rot, trans)

#==============================================

# create a view matrix using position, look-at vector and up vector
def getLookAtViewMatrix(pos, at, up):

    # normalize a vector
    def normalize(x):
        return x / np.linalg.norm(x)

    # create a 3D translation matrix
    def getTranslationMatrix(translation):
        m = np.eye(4)
        m[0:3, 3] = translation
        return m

    z = normalize(pos - at)
    x = normalize(np.cross(normalize(up), z))
    y = normalize(np.cross(z, x))
    rot = np.transpose(np.stack([x, y, z]))
    r = np.eye(4)
    r[0:3, 0:3] = rot
    t = getTranslationMatrix(pos)
    return np.linalg.inv(t.dot(r))


#==============================================

# create toe-in stereo matrices from a mono camera
def stereoFromMonoCam(camera, res, eyeSeparation=2, screenDepth=0.05):

    leftEye = copy.deepcopy(camera)
    rightEye = copy.deepcopy(camera)

    # view matrices
    pos = camera.camera_centerCPU
    r = camera.R
    up = r.dot(np.array([0, 1, 0]))
    at = pos + r.dot(np.array([0, 0, -1]))
        
    tVec = r.dot(np.array([0.5 * eyeSeparation, 0, 0]))
    leftView = getLookAtViewMatrix(pos - tVec, at - tVec * (1 - screenDepth), up)
    leftEye.updateCamera(np.transpose(leftView[0:3, 0:3]), leftView[0:3,3])
    rightView = getLookAtViewMatrix(pos + tVec, at + tVec * (1 - screenDepth), up)
    rightEye.updateCamera(np.transpose(rightView[0:3, 0:3]), rightView[0:3,3])

    # projection matrices
    focalLength = fov2focal(camera.FoVy, res[1])
    wd2 = camera.znear * math.tan(0.5 * camera.FoVy) 
    ndfl = camera.znear / focalLength
    aspect = res[0] / float(res[1])
    leftProj = getPatchProjectionMatrix(
        left=-aspect * wd2 + 0.5 * eyeSeparation * ndfl,
        right=aspect * wd2 + 0.5 * eyeSeparation * ndfl,
        top=wd2, 
        bottom=-wd2, 
        znear=camera.znear, 
        zfar=camera.zfar)
    rightProj = getPatchProjectionMatrix(
        left=-aspect * wd2 - 0.5 * eyeSeparation * ndfl,
        right=aspect * wd2 - 0.5 * eyeSeparation * ndfl,
        top=wd2, 
        bottom=-wd2, 
        znear=camera.znear, 
        zfar=camera.zfar)    
    leftEye.setProjection(leftProj)
    rightEye.setProjection(rightProj)

    return leftEye, rightEye
