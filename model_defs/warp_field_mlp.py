from torch import nn
import torch
from typing import List
import os
from utils.graphics_utils import geom_transform_points

class BasicMLP(nn.Module):
    def __init__(self, insize, outsize, width, num_layers, activate_last=False):
        super(BasicMLP, self).__init__()
        self.width = width
        self.num_layers = num_layers

        if num_layers>1:
            first_layer = nn.Linear(insize, width)
            nn.init.kaiming_uniform_(first_layer.weight, nonlinearity="relu")
            layers = [first_layer]  # , nn.ReLU()]
            for i in range(num_layers - 1):
                layer = nn.Linear(width, width)
                nn.init.kaiming_uniform_(layer.weight, nonlinearity="relu")
                layers.append(layer)
            self.lastlayer = nn.Linear(width, outsize, bias=True)
        else:
            layers=[]
            self.lastlayer = nn.Linear(insize, outsize, bias=True)
        nn.init.kaiming_uniform_(self.lastlayer.weight, nonlinearity="relu")

        if activate_last:
            self.lastactivate = nn.ReLU()
        else:
            self.lastactivate = nn.Identity()

        self.module_list = nn.ModuleList(layers)
        self.activate = nn.ReLU()

    def forward(self, x):
        for f in self.module_list:
            x = self.activate(f(x))
        return self.lastactivate(self.lastlayer(x))


class OneBlockMLP(nn.Module):
    def __init__(self, insize, outsize, width, num_layers):
        super(OneBlockMLP, self).__init__()
        self.mlp1 = BasicMLP(insize, outsize, width, num_layers, activate_last=False)

    def forward(self, inputs: List[torch.Tensor]):
        points_xyz = inputs[0]
        camera_center = inputs[1]
        x = torch.cat((camera_center, points_xyz), dim=1)
        return 0.01 * self.mlp1(x) + points_xyz

    def plot_histogram(self, tb_writer, path, step):
        for name, weights in self.named_parameters():
            tb_writer.add_histogram(os.path.join(path, "weights", name), weights, step)
            #tb.add_histogram(f'{name}.grad', weight.grad, epoch)


class NormalizedMLP_SE3(nn.Module):
    def __init__(self, width, num_layers, mean, std, cam_mean, cam_std):
        super(NormalizedMLP_SE3, self).__init__()
        print("MLP of width ", width, " and depth ", num_layers)
        self.mlp = BasicMLP(6, 6, width, num_layers, activate_last=False)
        self.m = mean
        self.s = std
        self.cam = cam_mean
        self.cas = cam_std

    def skew(self, x):
        x_skewed = torch.zeros((x.shape[0], 3, 3), device="cuda")
        x_skewed[:, 0, 1] = -x[:, 2]
        x_skewed[:, 0, 2] = x[:, 1]
        x_skewed[:, 1, 0] = x[:, 2]
        x_skewed[:, 1, 2] = -x[:, 0]
        x_skewed[:, 2, 0] = -x[:, 1]
        x_skewed[:, 2, 1] = x[:, 0]

        return x_skewed

    def get_batch_I(self, batch, n):
        return torch.eye(n, device="cuda").unsqueeze(0).repeat(batch, 1, 1)

    def forward(self, inputs: List[torch.Tensor]):
        points_xyz = (inputs[0] - self.m) / self.s
        camera_center = (inputs[1] - self.cam) / self.cas
        # x = torch.cat((self.mlp1(camera_center), points_xyz), dim=1)
        # return self.mlp2(x) * self.s + self.m
        x = torch.cat((camera_center, points_xyz), dim=1)

        w, v = (self.mlp(x)*0.0001).split(3, dim=1)
        theta = w.norm(2, dim=1, keepdim=True)
        w_h = w/theta
        v_h = v/theta

        W_h = self.skew(w_h)

        theta = theta.unsqueeze(-1)
        R = self.get_batch_I(x.shape[0], 3) + torch.sin(theta)*W_h + (1.0 - torch.cos(theta))*W_h @ W_h
        p = (theta*self.get_batch_I(x.shape[0], 3) + (1.0   - torch.cos(theta))*W_h +
                                                     (theta - torch.sin(theta))*W_h@W_h)@v_h.unsqueeze(-1)

        transf = torch.eye(4, device="cuda").repeat(x.shape[0], 1, 1)
        transf[:,:3,:3] = R
        transf[:,:3, 3:4] = p

        ones = torch.ones(points_xyz.shape[0], 1, dtype=points_xyz.dtype, device=points_xyz.device)
        points_hom = torch.cat([points_xyz, ones], dim=1)
        points_out = torch.matmul(transf, points_hom.unsqueeze(-1)).squeeze()

        denom = points_out[..., 3:] + 0.0000001
        return (points_out[..., :3] / denom).squeeze(dim=0) * self.s + self.m

    def plot_histogram(self, tb_writer, path, step):
        for name, weights in self.named_parameters():
            tb_writer.add_histogram(os.path.join(path, "weights", name), weights, step)
            #tb.add_histogram(f'{name}.grad', weight.grad, epoch)


class NormalizedMLP(nn.Module):
    def __init__(self, width, num_layers, mean, std, cam_mean, cam_std):
        super(NormalizedMLP, self).__init__()
        print("MLP of width ", width, " and depth ", num_layers)
        self.mlp = BasicMLP(6, 3, width, num_layers, activate_last=False)
        self.m = mean
        self.s = std
        self.cam = cam_mean
        self.cas = cam_std

    def forward(self, inputs: List[torch.Tensor]):
        points_xyz = (inputs[0] - self.m) / self.s
        camera_center = (inputs[1] - self.cam) / self.cas
        # x = torch.cat((self.mlp1(camera_center), points_xyz), dim=1)
        # return self.mlp2(x) * self.s + self.m
        x = torch.cat((camera_center, points_xyz), dim=1)
        return (0.01 * self.mlp(x) + points_xyz) * self.s + self.m

    def plot_histogram(self, tb_writer, path, step):
        for name, weights in self.named_parameters():
            tb_writer.add_histogram(os.path.join(path, "weights", name), weights, step)
            #tb.add_histogram(f'{name}.grad', weight.grad, epoch)



class TwoBlockMLP(nn.Module):
    def __init__(self, insize1, insize2, outsize, width, num_layers1, num_layers2):
        super(TwoBlockMLP, self).__init__()
        self.mlp1 = BasicMLP(insize1, width, width, num_layers1, activate_last=True)
        self.mlp2 = BasicMLP(width + insize2, outsize, width + insize2, num_layers2, activate_last=False)

    def forward(self, inputs: List[torch.Tensor]):
        points_xyz = inputs[0]
        camera_center = inputs[1]

        b = self.mlp1(points_xyz)
        return self.mlp2(torch.cat((camera_center, b), dim=1))


class CatacausticMLP(nn.Module):
    def __init__(self, width_param, width_embed, depth_param, depth_embed,mean, std, cam_mean, cam_std):
        super(CatacausticMLP, self).__init__()
        self.mlp_param = BasicMLP(3, 2, 16, 3, activate_last=False)
        self.mlp_embed = BasicMLP(5, 3, 256, 4, activate_last=False)
        # self.uv_activate = nn.Sigmoid()
        self.uv_activate = nn.Identity()
        self.uv = None
        self.raw_mlp_out = None
        self.output = None

        self.m = mean
        self.s = std
        self.cam = cam_mean
        self.cas = cam_std

    def normalize_points(self, p):
        return (p - self.m) / self.s
    
    def normalize_cams(self, c):
        return (c - self.cam) / self.cas

    def forward(self, inputs: List[torch.Tensor]):
        points_xyz = self.normalize_points(inputs[0])
        camera_center = self.normalize_cams(inputs[1])
        #input_param = torch.cat((camera_center, points_xyz), dim=1)
        #uv_raw = self.mlp_param(input_param)
        uv_raw = self.mlp_param(camera_center)
        self.uv = self.uv_activate(uv_raw)
        input_embed = torch.cat((self.uv, points_xyz), dim=1)
        #input_embed = self.uv
        self.raw_mlp_out = self.mlp_embed(input_embed)
        self.output = (points_xyz + 0.01 * self.raw_mlp_out)* self.s + self.m
        return self.output

    def plot_histogram(self, tb_writer, path, step):
        for name, weights in self.named_parameters():
            tb_writer.add_histogram(os.path.join(path, "weights", name), weights, step)
            #tb.add_histogram(f'{name}.grad', weight.grad, epoch)


class ProgressiveCatacausticMLP(nn.Module):
    def __init__(self, width_param, width_embed, depth_param, depth_embed,mean, std, cam_mean, cam_std):
        super(ProgressiveCatacausticMLP, self).__init__()
        self.mlp_param = BasicMLP(3, 3, 16, 2, activate_last=False)
        self.mlp_embed = BasicMLP(6, 3, 256, 4, activate_last=False)
        self.uvw_activate = nn.Tanh()
        #self.uv_activate = nn.Identity()
        self.uv = None
        self.uvw = None
        self.raw_mlp_out = None
        self.output = None

        self.m = mean
        self.s = std
        self.cam = cam_mean
        self.cas = cam_std

    def normalize_points(self, p):
        return (p - self.m) / self.s
    
    def normalize_cams(self, c):
        return (c - self.cam) / self.cas

    def sort_uvw(self, uvw):

        def permute_2d(x, permutation):
            d1, d2 = x.size()
            ret = x[
                torch.arange(d1).unsqueeze(1).repeat((1, d2)).flatten(),
                permutation.flatten()
            ].view(d1, d2)
            return ret

        uvw_sort_indices = torch.argsort(torch.abs(uvw), dim=1, descending=True)
        return permute_2d(uvw, uvw_sort_indices)

    def forward(self, inputs: List[torch.Tensor], w_damp_factor=0.1):
        
        # encoder
        # TODO: don't use expanded cameras, we likely just need one
        points_xyz = self.normalize_points(inputs[0])
        camera_center = self.normalize_cams(inputs[1])
        uvw_raw = self.mlp_param(camera_center)
        uvw = self.uvw_activate(uvw_raw)

        # sort 3D latent code and dampen the smallest value
        #uvw_sorted = self.sort_uvw(uvw)
        #uvw_sorted[:, 2] *= w_damp_factor

        uvw_sorted = uvw
        
        # make sure self.uv is in the pipeline so that we can differentiate w.r.t. it
        self.uv = uvw_sorted[:, :2]
        self.uvw = torch.cat((self.uv, uvw_sorted[:, 2:]), dim=1)

        # decoder
        input_embed = torch.cat((self.uvw, points_xyz), dim=1)
        self.raw_mlp_out = self.mlp_embed(input_embed)
        self.output = (points_xyz + 0.01 * self.raw_mlp_out)* self.s + self.m
        return self.output

    def plot_histogram(self, tb_writer, path, step):
        for name, weights in self.named_parameters():
            tb_writer.add_histogram(os.path.join(path, "weights", name), weights, step)
            #tb.add_histogram(f'{name}.grad', weight.grad, epoch)