import torch
import torch.nn.functional as F
from torch.autograd import Variable
from math import exp

def l1_loss(network_output, gt):
    return torch.abs((network_output - gt)).mean()

def l2_loss(network_output, gt):
    return ((network_output - gt) ** 2).mean()

def cata_loss(warp_field_mlp, camera, filter):
    filter = filter.detach()
    def normalizeVectors(v):
        return v / (v.norm(2, dim=1, keepdim=True) + 0.0001)

    # get the catacaustic surface normal
    num_samples = warp_field_mlp.output.shape[0]
    num_active_samples = warp_field_mlp.output[filter].shape[0]
    cata_surface_tangents = torch.zeros((num_samples, 2, 3), device="cuda")
    for dim in range(3):
        cata_surface_tangents[..., dim] = torch.autograd.grad(
            outputs=warp_field_mlp.raw_mlp_out[..., dim][filter],
            inputs=warp_field_mlp.uv,
            grad_outputs=torch.ones_like(warp_field_mlp.raw_mlp_out[..., dim][filter]),
            create_graph=True)[0]
    cata_surface_normals = normalizeVectors(torch.cross(cata_surface_tangents[:, 0, :][filter], cata_surface_tangents[:, 1, :][filter], dim=1))

    # penalize deviations from catacaustic tangent constraint
    cam_center_expand = camera.camera_center.expand(num_active_samples, 3)
    points_to_cam = normalizeVectors(warp_field_mlp.output[filter] - cam_center_expand)
    dot = torch.einsum('ab,ab->a', cata_surface_normals, points_to_cam)
    return torch.abs(dot).mean()

def tv_reg(img):
    w_variance = torch.sum(torch.pow(img[:, :, :, :-1] - img[:, :, :, 1:], 2))
    h_variance = torch.sum(torch.pow(img[:, :, :-1, :] - img[:, :, 1:, :], 2))
    return h_variance + w_variance


def gaussian(window_size, sigma):
    gauss = torch.Tensor([exp(-(x - window_size // 2) ** 2 / float(2 * sigma ** 2)) for x in range(window_size)])
    return gauss / gauss.sum()


def create_window(window_size, channel):
    _1D_window = gaussian(window_size, 1.5).unsqueeze(1)
    _2D_window = _1D_window.mm(_1D_window.t()).float().unsqueeze(0).unsqueeze(0)
    window = Variable(_2D_window.expand(channel, 1, window_size, window_size).contiguous())
    return window

def ssim(img1, img2, window_size=11, size_average=True):
    (_, channel, _, _) = img1.size()
    window = create_window(window_size, channel)

    if img1.is_cuda:
        window = window.cuda(img1.get_device())
    window = window.type_as(img1)

    return _ssim(img1, img2, window, window_size, channel, size_average)

def _ssim(img1, img2, window, window_size, channel, size_average=True):
    mu1 = F.conv2d(img1, window, padding=window_size // 2, groups=channel)
    mu2 = F.conv2d(img2, window, padding=window_size // 2, groups=channel)

    mu1_sq = mu1.pow(2)
    mu2_sq = mu2.pow(2)
    mu1_mu2 = mu1 * mu2

    sigma1_sq = F.conv2d(img1 * img1, window, padding=window_size // 2, groups=channel) - mu1_sq
    sigma2_sq = F.conv2d(img2 * img2, window, padding=window_size // 2, groups=channel) - mu2_sq
    sigma12 = F.conv2d(img1 * img2, window, padding=window_size // 2, groups=channel) - mu1_mu2

    C1 = 0.01 ** 2
    C2 = 0.03 ** 2

    ssim_map = ((2 * mu1_mu2 + C1) * (2 * sigma12 + C2)) / ((mu1_sq + mu2_sq + C1) * (sigma1_sq + sigma2_sq + C2))

    if size_average:
        return ssim_map.mean()
    else:
        return ssim_map.mean(1).mean(1).mean(1)

