import os
import sys
import math
from PIL import Image
import torch
from typing import NamedTuple
from scene_loaders.colmap_loader import read_extrinsics_text, read_intrinsics_text, qvec2rotmat, \
    read_extrinsics_binary, read_intrinsics_binary
from utils.graphics_utils import getWorld2View2, focal2fov, fov2focal
import numpy as np
import json
import open3d as o3d
from xml.dom import minidom
import functools
from pathlib import Path

class CameraInfo(NamedTuple):
    uid: int
    R: np.array
    T: np.array
    FovY: np.array
    FovX: np.array
    image: np.array
    image_path: str
    image_name: str

class SceneInfo(NamedTuple):
    point_cloud: o3d.geometry.PointCloud
    train_cameras: list
    validation_cameras: list
    nerf_normalization: dict

def getNerfppNorm(cam_info):
    def get_center_and_diag(cam_centers):
        cam_centers = np.hstack(cam_centers)
        avg_cam_center = np.mean(cam_centers, axis=1, keepdims=True)
        center = avg_cam_center
        dist = np.linalg.norm(cam_centers - center, axis=0, keepdims=True)
        diagonal = np.max(dist)
        return center.flatten(), diagonal

    cam_centers = []
    target_radius = 1.0

    for cam in cam_info:
        W2C = getWorld2View2(cam.R, cam.T).squeeze()
        C2W = np.linalg.inv(W2C)
        cam_centers.append(C2W[:3, 3:4])

    center, diagonal = get_center_and_diag(cam_centers)
    radius = diagonal * 1.1

    translate = -center
    scale = target_radius / radius

    return {"translate": translate, "scale": scale}

def readFvsSceneInfo(name, path):
    ply_path = os.path.join(path, "dense/delaunay_photometric.ply")
    files_path = os.path.join(path, "dense/ibr3d_pw_0.50/")
    Ks = np.load(os.path.join(files_path, "Ks.npy"))
    Rs = np.load(os.path.join(files_path, "Rs.npy"))
    Ts = np.load(os.path.join(files_path, "ts.npy"))
    num_cameras = Ts.shape[0]
    cameras = []
    for idx in range(num_cameras):
        sys.stdout.write('\r')
        # the exact output you're looking for:
        sys.stdout.write("Reading \"{}\" | Camera {}/{}".format(name, idx, num_cameras))
        sys.stdout.flush()

        uid_str = format(idx, '08d')

        image_name = "im_" + uid_str
        image_path = os.path.join(files_path, image_name + ".jpg")
        image = Image.open(image_path)
        image = torch.from_numpy(np.array(image)) / 255.0
        image = image.permute(2, 0, 1).unsqueeze(dim=0)

        height = image.shape[2]
        width = image.shape[3]
        focal_length_x = Ks[idx, 0, 0]
        focal_length_y = Ks[idx, 1, 1]
        FovY = 2.0 * math.atan(0.5 * height / focal_length_y)
        FovX = 2.0 * math.atan(0.5 * width / focal_length_x)

        cam_info = CameraInfo(uid=idx, R=np.transpose(Rs[idx]), T=Ts[idx], FovY=FovY, FovX=FovX, image=image,
                              image_path=image_path, image_name=image_name)
        cameras.append(cam_info)
    sys.stdout.write('\n')

    scene_info = SceneInfo(ply_path=ply_path,
                           num_cameras=num_cameras,
                           cameras=cameras)
    return scene_info


def readColmapSceneInfo(path, NerfNormalize=False):

    try:
        cameras_extrinsic_file = os.path.join(path, "colmap/stereo/sparse/images.txt")
        cameras_intrinsic_file = os.path.join(path, "colmap/stereo/sparse/cameras.txt")
        cam_extrinsics = read_extrinsics_text(cameras_extrinsic_file)
        cam_intrinsics = read_intrinsics_text(cameras_intrinsic_file)
    except:
        cameras_extrinsic_file = os.path.join(path, "colmap/stereo/sparse/images.bin")
        cameras_intrinsic_file = os.path.join(path, "colmap/stereo/sparse/cameras.bin")
        cam_extrinsics = read_extrinsics_binary(cameras_extrinsic_file)
        cam_intrinsics = read_intrinsics_binary(cameras_intrinsic_file)
    #assert (len(cam_extrinsics) == len(cam_intrinsics))
    num_cameras = len(cam_extrinsics)
    cameras = []
    for idx, key in enumerate(cam_extrinsics):
        sys.stdout.write('\r')
        # the exact output you're looking for:
        sys.stdout.write("Reading Files | Camera {}/{}".format(idx, len(cam_extrinsics)))
        sys.stdout.flush()
        # if key not in cam_intrinsics:
        #    continue
        extr = cam_extrinsics[key]
        intr = cam_intrinsics[extr.camera_id]
        height = intr.height
        width = intr.width

        uid = intr.id
        R = np.transpose(qvec2rotmat(extr.qvec))
        T = np.array(extr.tvec)

        if intr.model=="SIMPLE_PINHOLE":
            focal_length_x = intr.params[0]
            #focal_length_y = intr.params[1]
            FovY = focal2fov(focal_length_x, height)
            FovX = focal2fov(focal_length_x, width)
        elif intr.model=="PINHOLE":
            focal_length_x = intr.params[0]
            focal_length_y = intr.params[1]
            FovY = focal2fov(focal_length_y, height)
            FovX = focal2fov(focal_length_x, width)
        else:
            assert False, "Colmap camera model not handled!"

        image_path = os.path.join(path, "colmap/stereo/images", extr.name)
        image_name = os.path.basename(image_path).split(".")[0]
        image = Image.open(image_path)

        cam_info = CameraInfo(uid=uid, R=R, T=T, FovY=FovY, FovX=FovX, image=image,
                              image_path=image_path, image_name=image_name)
        cameras.append(cam_info)
        # photometric_depth_map = torch.tensor(
        #    read_colmap_bin_array(os.path.join(path, "colmap/stereo/stereo/depth_maps/",
        #                                       extr.name + ".photometric.bin"))).unsqueeze(dim=0).unsqueeze(dim=0)
        # geometric_depth_map = torch.tensor(
        #    read_colmap_bin_array(os.path.join(path, "colmap/stereo/stereo/depth_maps/",
        #                                       extr.name + ".geometric.bin"))).unsqueeze(dim=0).unsqueeze(dim=0)
    sys.stdout.write('\n')

    nerf_normalization = {"translate": [0.0, 0.0, 0.0], "scale": 1.0}
    if NerfNormalize:
        nerf_normalization = getNerfppNorm(cameras)

    pcd = o3d.io.read_point_cloud(os.path.join(path, "colmap/stereo/fused.ply"))

    scene_info = SceneInfo(point_cloud=pcd,
                           num_cameras=num_cameras,
                           cameras=cameras,
                           nerf_normalization=nerf_normalization)
    return scene_info

def readNVMFile(path):
    with open(path) as fp:
        lines = fp.readlines()
        nvm_version = lines.pop(0)
        l = lines.pop(0)
        while l == '\n':
            l = lines.pop(0)
        num_cams = int(l)
        cameras = []
        for idx in range(num_cams):
            l = lines.pop(0).split(' ')
            image_name = l[0]
            focal_length = float(l[1])
            qvec = np.array([float(l[2]), float(l[3]),
                             float(l[4]), float(l[5])])
            pos = np.array([float(l[6]), float(l[7]), float(l[8])])
            radial_distortion = int(l[9])
            assert (radial_distortion == 0 and int(l[10]) == 0)
            cameras.append((image_name, focal_length, qvec, pos))
        return cameras

def readDeepBlending(name, path):
    ply_path = os.path.join(path, "global_mesh_with_texture", "mesh.ply")
    cams = readNVMFile(os.path.join(path, "input_camera_poses_as_nvm", "scene.nvm"))
    cameras = []
    num_cameras = len(cams)
    for idx, cam in enumerate(cams):
        sys.stdout.write('\r')
        # the exact output you're looking for:
        sys.stdout.write("Reading \"{}\" | Camera {}/{}".format(name, idx, num_cameras))
        sys.stdout.flush()

        image_path = os.path.join(path, "input_camera_poses_as_nvm", cam[0])
        image_name = os.path.basename(image_path)
        image = Image.open(image_path)
        image = torch.from_numpy(np.array(image)) / 255.0
        image = image.permute(2, 0, 1).unsqueeze(dim=0)

        height = image.shape[2]
        width = image.shape[3]
        focal_length = cam[1]
        FovY = 2.0 * math.atan(0.5 * height / focal_length)
        FovX = 2.0 * math.atan(0.5 * width / focal_length)

        R = np.transpose(qvec2rotmat(cam[2]))
        origin = cam[3]
        T = -np.matmul(np.linalg.inv(R), origin)

        cam_info = CameraInfo(uid=idx, R=R, T=T, FovY=FovY, FovX=FovX, image=image,
                              image_path=image_path, image_name=image_name)
        cameras.append(cam_info)
    sys.stdout.write('\n')
    scene_info = SceneInfo(ply_path=ply_path,
                           num_cameras=num_cameras,
                           cameras=cameras)
    return scene_info

def readMitsuba(path, NerfNormalize=False):
    with open(os.path.join(path, "pbnrScene", "cameras.json")) as json_file:
        cameras = json.load(json_file)["cameras"]
    num_cameras = len(cameras)
    cam_infos = []
    for idx, cam in enumerate(cameras):
        sys.stdout.write('\r')
        # the exact output you're looking for:
        sys.stdout.write("Reading Files | Camera {}/{}".format(idx, num_cameras))
        sys.stdout.flush()

        image_path = os.path.join(path, "pbnrScene", "images", cam["id"] + ".png")
        image_name = cam["id"]
        image = Image.open(image_path)

        height = image.size[1]
        width = image.size[0]
        FovX = cam["fovX"]*(math.pi/180.0)
        focal = fov2focal(FovX, width)
        FovY = focal2fov(focal, height)

        convert = np.eye(3)
        convert[0,0] = -1
        convert[1,1] = -1
        R = np.transpose(np.array(cam["R"]))@convert
        T = np.array(cam["t"])

        cam_infos.append(CameraInfo(uid=idx, R=R, T=T, FovY=FovY, FovX=FovX, image=image,
                                    image_path=image_path, image_name=image_name))

        #if idx >= 20:
        #    break

    sys.stdout.write('\n')

    nerf_normalization = {"translate": [0.0, 0.0, 0.0], "scale": 1.0}
    if NerfNormalize:
        nerf_normalization = getNerfppNorm(cam_infos)

    ### Read mitsuba xml to get diffuse point cloud
    objs = [x for x in [x.attributes["value"].value for x in
                        minidom.parse(os.path.join(path, "scene.xml")).getElementsByTagName("string") if
                        x.attributes["name"].value == "filename"] if x.endswith(".obj")]

    meshes = [o3d.io.read_triangle_mesh(os.path.join(path, x)) for x in objs]
    final_mesh = functools.reduce(lambda a, b: a + b, meshes)
    """
    plane = o3d.io.read_triangle_mesh("F:/gkopanas/pointbasedNerf/scenes\mitsuba/living-room-curved/torus-morecurve_triangle.obj")
    #plane = o3d.io.read_triangle_mesh("F:/gkopanas/pointbasedNerf/scenes\mitsuba/living-room/models/Plane_rot.obj")
    vertices = np.asarray(plane.vertices)
    normals = np.asarray(plane.vertex_normals)
    r0 = vertices.mean(axis=0)
    n = normals.mean(axis=0)/np.linalg.norm(normals.mean(axis=0))
    a, b, c = n
    d = -r0.dot(n)

    H = np.array([[1 - 2 * a * a, -2 * a * b, -2 * a * c, -2 * a * d],
                  [-2 * a * b, 1 - 2 * b * b, -2 * b * c, -2 * b * d],
                  [-2 * a * c, -2 * b * c, 1 - 2 * c * c, -2 * c * d],
                  [0.0, 0.0, 0.0, 1.0]])

    final_mesh = final_mesh.transform(H.transpose())
    """
    pcd = final_mesh.sample_points_uniformly(int(5.0 * 10 ** 6))
    pcd.colors = o3d.utility.Vector3dVector(0.5 * np.ones_like(pcd.points))

    scene_info = SceneInfo(point_cloud=pcd,
                           num_cameras=num_cameras,
                           cameras=cam_infos,
                           nerf_normalization=nerf_normalization)
    return scene_info

def readBunderFolder(cameras_folder, extension=".png", name_ints=8):
    cam_infos = []
    with open(os.path.join(cameras_folder, "bundle.out")) as bundle_file:
        # First line is a comment
        _ = bundle_file.readline()
        num_cameras, _ = [int(x) for x in bundle_file.readline().split()]
        for idx in range(num_cameras):
            cam_name = '{num:0{width}}'.format(num=idx, width=name_ints) + extension
            sys.stdout.write('\r')
            sys.stdout.write("{}/{}".format(idx, num_cameras))
            sys.stdout.flush()

            focal, dist0, dist1 = [float(x) for x in bundle_file.readline().split()]
            R = []
            for i in range(3):
                R.append([float(x) for x in bundle_file.readline().split()])
            T = [float(x) for x in bundle_file.readline().split()]

            convert = np.eye(3)
            convert[1, 1] = -1
            convert[2, 2] = -1
            R = np.transpose(np.array(R).reshape(3, 3)) @ convert
            T = np.array(T) @ convert

            image_path = os.path.join(cameras_folder, cam_name)
            image_name = Path(cam_name).stem
            image = Image.open(image_path)

            FovY = focal2fov(focal, image.size[1])
            FovX = focal2fov(focal, image.size[0])

            cam_infos.append(CameraInfo(uid=idx, R=R, T=T, FovY=FovY, FovX=FovX, image=image,
                                        image_path=image_path, image_name=image_name))
    print()
    return cam_infos

def readRealityCapture(path, NerfNormalize=False, extension=".png"):
    train_cameras_folder = os.path.join(path, "cropped_train_cameras")
    validation_cameras_folder = os.path.join(path, "validation_cameras")

    print("Reading Training Bundle File")
    train_cam_infos = readBunderFolder(train_cameras_folder)
    print("Reading Validation Bundle File")
    validation_cam_infos = readBunderFolder(validation_cameras_folder, name_ints=5)

    nerf_normalization = {"translate": [0.0, 0.0, 0.0], "scale": 1.0}

    print("Reading Point-Cloud...")
    if not os.path.exists(os.path.join(path, "meshes", "dense_point_cloud.ply")):
        print("Found only XYZ, convert to PLY, will happen once...")
        with open(os.path.join(path, "meshes", "dense_point_cloud.xyz")) as point_cloud_file:
            pc_string = point_cloud_file.read()
            data_np = np.array([float(item) for item in pc_string.split()]).reshape((-1, 9))
            xyz = (data_np[:, 0:3] + nerf_normalization["translate"])*nerf_normalization["scale"]
            normals = data_np[:, 3:6]
            colors = data_np[:, 6:9] / 255.0
            pcd = o3d.geometry.PointCloud()
            pcd.points = o3d.utility.Vector3dVector(xyz)
            pcd.normals = o3d.utility.Vector3dVector(normals)
            pcd.colors = o3d.utility.Vector3dVector(colors)
            o3d.io.write_point_cloud(os.path.join(path, "meshes", "dense_point_cloud.ply"), pcd)
    pcd = o3d.io.read_point_cloud(os.path.join(path, "meshes", "dense_point_cloud.ply"))
    print("Done...")

    scene_info = SceneInfo(point_cloud=pcd,
                           train_cameras=train_cam_infos,
                           validation_cameras=validation_cam_infos,
                           nerf_normalization=nerf_normalization)
    return scene_info


sceneLoadTypeCallbacks = {
    "FVS": readFvsSceneInfo,
    "Colmap": readColmapSceneInfo,
    "DB": readDeepBlending,
    "Mitsuba": readMitsuba,
    "RealityCapture": readRealityCapture
}