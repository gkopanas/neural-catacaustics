import math
import torch
from torch import nn
import numpy as np
from utils.graphics_utils import geom_transform_points,getWorld2View2,\
    getProjectionMatrix, getPatchProjectionMatrix, getNormalisedImageGrid
from random import randint

def getSimpleCamera(R, t, fovY, fovX, h, w, zfar, znear):
    # fovy = fov
    # fovx = fov

    pixel_size_y = (2 * math.tan(fovY / 2)) / h
    pixel_size_x = (2 * math.tan(fovX / 2)) / w

    cx = w / 2
    cy = h / 2
    top_pixel = cy
    bottom_pixel = top_pixel - h
    right_pixel = cx
    left_pixel = right_pixel - w
    camera = CameraSimple(R=R, T=t,
                          top=top_pixel * pixel_size_y, bottom=bottom_pixel * pixel_size_y,
                          right=right_pixel * pixel_size_x, left=left_pixel * pixel_size_x,
                          znear=znear,
                          zfar=zfar,
                          image_height=h,
                          image_width=w,
                          scale=np.ones(1),
                          trans=np.zeros(3),
                          fovy=fovY,
                          fovx=fovX)

    return camera


class PulsarCamera(nn.Module):
    def __init__(self, cam_params, image_height, image_width, patch):
        super(PulsarCamera, self).__init__()

        self.cam_params = cam_params
        self.image_height = image_height
        self.image_width = image_width

        self.patch = patch


class CameraSimple(nn.Module):
    def __init__(self, R, T, top, bottom, right, left, znear, zfar, image_height, image_width, trans, scale, fovx=None,
                 fovy=None):
        super(CameraSimple, self).__init__()
        self.zfar = zfar
        self.znear = znear

        self.FoVx = fovx
        self.FoVy = fovy

        self.trans = trans
        self.scale = scale

        self.image_height = int(image_height)
        self.image_width = int(image_width)

        # Add real zfar znear here?
        self.setProjection(getPatchProjectionMatrix(
            znear=znear, zfar=zfar, top=top, bottom=bottom, right=right, left=left)
        )

        self.updateCamera(R, T)

        # if fovx is not None:
        #    self.image = torch.zeros(1, 3, self.image_height, self.image_width)
        #    self.ray_sampler = create_sampler(self)
        #    self.rays_o, self.rays_d, _ = get_rays_single_image(self.ray_sampler.H, self.ray_sampler.W,
        #                                                        self.ray_sampler.intrinsics, self.ray_sampler.c2w_mat)
        #    self.rays_d = torch.tensor(self.rays_d.astype(dtype=np.float32)).permute((1,0)).view((1, 3, self.ray_sampler.H, self.ray_sampler.W)).cuda()

    def updateCamera(self, R, T):
        self.R = R
        self.T = T
        self.world_view_transformCPU = getWorld2View2(R, T, self.trans, self.scale)
        self.world_view_transform = torch.tensor(self.world_view_transformCPU).transpose(1, 2).cuda()
        self.full_proj_transform = self.world_view_transform.bmm(self.projection_matrix)
        self.camera_center = self.world_view_transform.inverse()[:, 3, :3]
        self.camera_centerCPU = self.camera_center[0].cpu().numpy()


    def setProjection(self, projection_matrix):
        self.projection_matrixCPU = projection_matrix
        self.projection_matrix = self.projection_matrixCPU.transpose(1, 2).cuda()

    
class Camera(nn.Module):
    def __init__(self, colmap_id, R, T, FoVx, FoVy, image, gt_alpha_mask, polytope_mask,
                 image_name, max_radius, uid,
                 trans=np.array([0.0, 0.0, 0.0]), scale=1.0
                 ):
        super(Camera, self).__init__()

        self.neighbors = None
        self.uid = uid
        self.colmap_id = colmap_id
        self.R = R
        self.T = T
        self.FoVx = FoVx
        self.FoVy = FoVy

        self.original_image = image.clamp(0.0, 1.0).cuda()

        self.image_width = self.original_image.shape[3]
        self.image_height = self.original_image.shape[2]
        self.image_name = image_name

        if gt_alpha_mask is not None:
            self.gt_alpha_mask = gt_alpha_mask.cuda()
        else:
            self.gt_alpha_mask = torch.ones((1, 1, self.image_height, self.image_width), device="cuda")

        if polytope_mask is not None:
            self.polytope_mask = polytope_mask.cuda()
        self.max_radius = int(max_radius)

        #self.exposure_coef = nn.Parameter(torch.ones(1, device="cuda").requires_grad_(True))

        #self.point_grid = getNormalisedImageGrid(self.image_height, self.image_width).cuda()

        # self.depth_map = loaded_depthmap.cuda() * scale
        # self.normal_map = loaded_normalmap.cuda()

        self.zfar = 100.0# self.getDepth().max()*1.2
        self.znear = 0.01# self.getDepth()[self.getDepth() > 0].min()/1.2

        self.trans = trans
        self.scale = scale

        self.world_view_transform = torch.tensor(getWorld2View2(R, T, trans, scale)).transpose(1, 2).cuda()
        self.projection_matrix = getProjectionMatrix(znear=self.znear, zfar=self.zfar, fovX=self.FoVx, fovY=self.FoVy).transpose(1,2).cuda()
        self.full_proj_transform = self.world_view_transform.bmm(self.projection_matrix)
        self.camera_center = self.world_view_transform.inverse()[:, 3, :3]

    def getDepth(self):
        return self.depth_map

    def getDirRays2(self):
        d = torch.ones_like(self.depth_map)
        xyz = self.getPointsFromDepthMap(d)
        ray_d = xyz - self.world_view_transform[0, 3, :3]
        return ray_d/torch.norm(ray_d, dim=0, keepdim=True)

    def getDirRays(self):
        width = self.image.shape[3]
        height = self.image.shape[2]

        points = getNormalisedImageGrid(height, width).to("cuda")
        pointsA = points.permute(1,2,0).view(height*width, 3)

        K = self.projection_matrix[:, :3, :3]
        K[0, 2, 2] = 1.0  # This removes the z component of the trasnformation because z is already at view space.

        Kinv = K.inverse()
        points_proj_inv = torch.matmul(pointsA, Kinv).squeeze(0)
        dirs = geom_transform_points(points_proj_inv, self.world_view_transform.inverse())
        dirs = dirs.view(height, width, 3)
        return dirs/torch.norm(dirs, dim=2, keepdim=True)

    def computeCovariance(self, point_cloud, normal, novel_view):
        novel_view_jacobian = self.computeJacobian(point_cloud, normal, novel_view)
        in_view_jacobian = self.computeJacobian(point_cloud, normal, self)

        full_jacobian = torch.bmm(in_view_jacobian.inverse(), novel_view_jacobian)

        covariance = torch.bmm(full_jacobian, full_jacobian.transpose(1,2))
        return covariance

    def getImageScaled(self, scale):
        return self.images[scale]

    def getPointsFromDepthMap(self, depth_map):
        pixel_grid = self.point_grid.to("cuda")
        points = depth_map*pixel_grid
        pointsA = points.permute(0,2,3,1).view(points.shape[2]*points.shape[3], points.shape[1])

        K = self.projection_matrix[:, :3, :3]
        K[0,2,2] = 1.0 # This removes the z component of the transformation because z is already at view space.
        Kinv = K.inverse()
        points_proj_inv = torch.matmul(pointsA, Kinv).squeeze(0)
        points_wc = geom_transform_points(points_proj_inv, self.world_view_transform.inverse())

        return points_wc

    def getPointsFromDepthMap3(self, depth_map):
        points_wc = -depth_map*self.getDirRays3() + self.camera_center.view(1,3,1,1)
        return points_wc.permute(0,2,3,1).view(-1, 3)

    def getPointsFromDepthMap2(self, depth_map=None, patch=None):
        if patch == None:
            patch = (0, 0, self.image_width, self.image_height)
        patch_origin_x, patch_origin_y, patch_size_x, patch_size_y = patch

        dir_rays = self.getDirRays3()[:, :,
                                      patch_origin_y:patch_origin_y + patch_size_y,
                                      patch_origin_x:patch_origin_x + patch_size_x]
        if not depth_map:
            depth_map = self.getDepth()
        points_wc = -depth_map*dir_rays + self.camera_center.view(1,3,1,1)
        return points_wc

    def getOrientation(self):
        viewMatrix = self.world_view_transform.cpu().numpy()[0]
        vm_inv = np.linalg.inv(viewMatrix)
        up = vm_inv[0,:3]
        right = vm_inv[1,:3]
        lookat = vm_inv[2,:3]
        return up, right, lookat

    def getRandomPatch(self, patch_size, polytope_only):
        width_min = 0
        width_max = self.image_width
        height_min = 0
        height_max = self.image_height
        if polytope_only:
            height_min, width_min = self.polytope_mask[0, 0, :, :].nonzero().min(dim=0).values
            height_max, width_max = self.polytope_mask[0, 0, :, :].nonzero().max(dim=0).values + 1

            # add 10% margin
            height_min = max(height_min - int(self.polytope_mask.shape[2]*0.05), 0)
            height_max = min(height_max + int(self.polytope_mask.shape[2]*0.05), self.polytope_mask.shape[2])
            width_min = max(width_min - int(self.polytope_mask.shape[3]*0.05), 0)
            width_max = min(width_max + int(self.polytope_mask.shape[3]*0.05), self.polytope_mask.shape[3])

            diff_w = patch_size - (width_max - width_min)
            diff_h = patch_size - (height_max - height_min)
            if diff_w > 0:
                width_min = width_min - (diff_w//2.0 + 1)
                width_max = width_max + (diff_w//2.0 + 1)
            if diff_h > 0:
                height_min = height_min - (diff_h//2.0 + 1)
                height_max = height_max + (diff_h//2.0 + 1)
        return (randint(width_min, width_max - patch_size),
                randint(height_min, height_max - patch_size),
                patch_size, patch_size)

