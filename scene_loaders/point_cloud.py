import torch
import numpy as np
from utils.general_utils import inverse_sigmoid, inverse_sigmoid_scaled, polar2cart
from torch import nn
import os
from utils.system_utils import mkdir_p
import open3d as o3d
from plyfile import PlyData, PlyElement
import subprocess
import time
from utils.cgal_utils import cgal_executable_path
import math

class PointCloud:
    def __init__(self, extra_features):
        self.extra_features = extra_features
        self._global_xyz = [torch.empty(0)]
        self._global_features = [torch.empty(0)]
        self._global_uncertainty = [torch.empty(0)]
        self._global_alpha = [torch.empty(0)]
        self._global_normals = [torch.empty(0)]
        self.o3d_pc = None
        self.ref_o3d_pc = None
        self._global_sigmoid_scale_u = [torch.empty(0)]
        self.gradient_accum = None

        self.lr_xyz = 0
        self.lr_u = 0
        self.lr_feat = 0
        self.lr_alpha = 0
        self.lr_normal = 0

        self.optimizer = None

    @property
    def global_xyz(self):
        return torch.cat(self._global_xyz, dim=0)
    @property
    def global_features(self):
        return torch.cat(self._global_features, dim=0)
    @property
    def global_uncertainty(self):
        return torch.cat(self._global_uncertainty, dim=0)
    @property
    def global_alpha(self):
        return torch.cat(self._global_alpha, dim=0)
    @property
    def global_normals(self):
        return torch.cat(self._global_normals, dim=0)
    @property
    def global_sigmoid_scale_u(self):
        return torch.cat(self._global_sigmoid_scale_u, dim=0)

    def get_feat_grads(self):
        return torch.cat([x.grad for x in self._global_features], dim=0)

    def get_xyz_grads(self):
        return torch.cat([x.grad for x in self._global_xyz], dim=0)

    def getDistances(self, pcd, num_neighbors):
        u = []
        pcd_tree = o3d.geometry.KDTreeFlann(pcd)
        for p in pcd.points:
            [_, _, d] = pcd_tree.search_knn_vector_3d(p, num_neighbors + 1)
            u.append(np.mean(d[1:]))
        return u

    def initialize_with_pcd(self, ref_o3d_pcd, lr_xyz, lr_feat,
                            lr_u, lr_alpha, lr_normal, apply_lr,
                            every_kth_point=None, normals=True):
        # # Jitter point positions
        # if not normals:
        #     fused_mean = np.asarray(ref_o3d_pcd.points).mean(axis=0)
        #     newxyz = np.asarray(ref_o3d_pcd.points)
        #     newxyz -= np.random.rand(newxyz.shape[0], 1) * 0.1 * (np.asarray(ref_o3d_pcd.points) - fused_mean)
        #     ref_o3d_pcd.points = o3d.utility.Vector3dVector(newxyz)
        self.ref_o3d_pc = ref_o3d_pcd
        self.initial_point_count = np.asarray(ref_o3d_pcd.points).shape[0]
        self.o3d_pc = self.ref_o3d_pc
        if every_kth_point is not None:
            print("Sampling PC to reach {} points ", every_kth_point)
            self.o3d_pc = ref_o3d_pcd.uniform_down_sample(every_kth_point)

        fused_point_cloud = torch.tensor(np.asarray(self.o3d_pc.points)).float().cuda()
        fused_normals = torch.tensor(np.asarray(self.o3d_pc.normals)).float().cuda()
        fused_color = torch.tensor(np.asarray(self.o3d_pc.colors)).float().cuda()
        features = inverse_sigmoid(torch.cat((fused_color, 0.5 * torch.ones((fused_color.shape[0], self.extra_features), device="cuda")),
                                    dim=1))
        # features = (2.0 * torch.rand(fused_point_cloud.shape[0], 3 + self.extra_features) - 1.0).cuda()
        print("Number of points at initialisation : ", fused_point_cloud.shape[0])

        dist2 = self.getDistances(self.o3d_pc, 3)
        dist2_clipped = np.clip(dist2,
                                a_min=max(np.percentile(dist2, 0.01), 0.00000001),
                                a_max=np.percentile(dist2, 70))
        sigmoid_scale = 4.0*dist2_clipped
        uncertainty = inverse_sigmoid_scaled(dist2_clipped, sigmoid_scale)

        uncertainty = torch.tensor(uncertainty, dtype=torch.float, device="cuda").unsqueeze(-1)
        alpha = inverse_sigmoid(0.5 * torch.ones((fused_point_cloud.shape[0], 1), dtype=torch.float, device="cuda"))

        self.active_normals = normals

        zero_normal_filter = ~(fused_normals == 0).all(dim=1)
        print("Filtered {} zero-normal points".format((~zero_normal_filter).sum()))
        self._global_normals = [nn.Parameter(fused_normals[zero_normal_filter].requires_grad_(True))]
        # Clean up point cloud
        self.o3d_pc.points = o3d.utility.Vector3dVector(fused_point_cloud[zero_normal_filter].cpu().numpy())

        self._global_xyz = [nn.Parameter(fused_point_cloud[zero_normal_filter].requires_grad_(True))]
        self._global_features = [nn.Parameter(features[zero_normal_filter].requires_grad_(True))]
        self._global_uncertainty = [uncertainty[zero_normal_filter]]
        self._global_sigmoid_scale_u = [torch.tensor(sigmoid_scale, dtype=torch.float, device="cuda").unsqueeze(-1)[zero_normal_filter]]
        self._global_alpha = [nn.Parameter(alpha[zero_normal_filter].requires_grad_(True))]

        self.gradient_accum = torch.zeros((self.global_xyz.shape[0], 1), device="cuda")
        self.denom = torch.zeros((self.global_xyz.shape[0], 1), device="cuda")

        self.lr_xyz = lr_xyz
        self.lr_u = lr_u
        self.lr_feat = lr_feat
        self.lr_alpha = lr_alpha
        self.lr_normal = lr_normal
        self.optimizer = torch.optim.Adam([
            {'params': self._global_xyz, 'lr': self.lr_xyz if apply_lr else 0.0},
            {'params': self._global_uncertainty, 'lr': self.lr_u if apply_lr else 0.0},
            {'params': self._global_features, 'lr': self.lr_feat},
            {'params': self._global_alpha, 'lr': self.lr_alpha},
            {'params': self._global_normals, 'lr': self.lr_normal if apply_lr else 0.0}
        ], lr=0.0, eps=1e-15)

    def densify_fast(self, resolution_scale, lr_flag, normals=False, propagate_position=False):
        with torch.no_grad():
            if resolution_scale > 1:
                print("Start densification at sampling rate 1 out of ", round(resolution_scale ** 3))
                new_o3d_pcd = self.ref_o3d_pc.uniform_down_sample(round(resolution_scale ** 3))
            else:
                new_o3d_pcd = self.ref_o3d_pc

            new_number_points = np.asarray(new_o3d_pcd.points).shape[0]
            if propagate_position:
                old_point_cloud = torch.tensor(np.asarray(self.o3d_pc.points)).float().cuda()

            fused_point_cloud = torch.tensor(np.asarray(new_o3d_pcd.points)).float().cuda()
            fused_normals = torch.tensor(np.asarray(new_o3d_pcd.normals)).float().cuda()
            print("New number of points: ", new_number_points)

            print("Building NN structure...")
            pcd_tree = o3d.geometry.KDTreeFlann(self.o3d_pc)
            
            print("Finding neighbors...")
            num_neighbors = 4  # 4 or 6

            neigh_indices = np.zeros((new_number_points, num_neighbors), dtype=np.int32)
            neigh_d = np.zeros((new_number_points, num_neighbors), dtype=np.float32)

            for i in range(new_number_points):
                _, idx, d = pcd_tree.search_knn_vector_3d(new_o3d_pcd.points[i], num_neighbors)
                neigh_indices[i] = idx
                neigh_d[i] = d

            neigh_d = torch.tensor(neigh_d).float().cuda()
            neigh_indices = torch.tensor(neigh_indices).long().cuda()

            print("Propagating information to higher-resolution point cloud...")
            inv_d = 1.0 / (neigh_d + 0.00001)
            weights = inv_d[..., None] / torch.sum(inv_d, dim=1)[:, None, None]
            feats_neigh = torch.sigmoid(self.global_features[neigh_indices]) * weights
            features = torch.sum(feats_neigh, dim=1)
            alpha_neigh = torch.sigmoid(self.global_alpha[neigh_indices]) * weights
            alpha = torch.sum(alpha_neigh, dim=1)
            if propagate_position:
                fused_point_cloud_neigh = (self.global_xyz[neigh_indices] - old_point_cloud[neigh_indices]) * weights
                fused_point_cloud += torch.sum(fused_point_cloud_neigh, dim=1)
                if normals:
                    fused_normals_neigh = self.global_normals[neigh_indices] * weights
                    fused_normals += torch.sum(fused_normals_neigh, dim=1)
                    
            dist2 = self.getDistances(new_o3d_pcd, num_neighbors)
            dist2_clipped = np.clip(dist2,
                                    a_min=0.00000001,
                                    a_max=np.percentile(dist2, 90))
            sigmoid_scale = 4.0*dist2_clipped
            uncertainty = inverse_sigmoid_scaled(dist2_clipped, sigmoid_scale)
            sigmoid_scale = sigmoid_scale[..., np.newaxis]
            uncertainty = uncertainty[..., np.newaxis]
            uncertainty = torch.tensor(uncertainty, dtype=torch.float, device="cuda")

            #uncertainty = 0.125 * uncertainty
            #sigmoid_scale = 2.0 * uncertainty
            #uncertainty = torch.log(uncertainty / (sigmoid_scale - uncertainty))
            self._global_sigmoid_scale_u = [torch.tensor(sigmoid_scale, dtype=torch.float, device="cuda")]

            print("Finished nearest neighbor blending")
            self._global_xyz = [nn.Parameter(fused_point_cloud.requires_grad_(True))]
            self._global_features = [nn.Parameter(inverse_sigmoid(features).requires_grad_(True))]
            self._global_uncertainty = [uncertainty]
            self._global_alpha = [nn.Parameter(inverse_sigmoid(alpha).requires_grad_(True))]
            fused_normals /= torch.sqrt(torch.sum((fused_normals ** 2), dim=1, keepdim=True))
            self._global_normals = [nn.Parameter(fused_normals.requires_grad_(True))]

            self.gradient_accum = torch.zeros((self.global_xyz.shape[0], 1), device="cuda")
            self.denom = torch.zeros((self.global_xyz.shape[0], 1), device="cuda")

            self.o3d_pc = new_o3d_pcd

            self.optimizer = torch.optim.Adam([
                {'params': self._global_xyz, 'lr': self.lr_xyz if lr_flag else 0.0},
                {'params': self._global_uncertainty, 'lr': self.lr_u if lr_flag else 0.0},
                {'params': self._global_features, 'lr': self.lr_feat},
                {'params': self._global_alpha, 'lr': self.lr_alpha},
                {'params': self._global_normals, 'lr': self.lr_normal if lr_flag else 0.0}
            ], lr=0.0, eps=1e-15)

    def densify(self, resolution_scale, normals=False, propagate_position=False):
        with torch.no_grad():
            if resolution_scale > 1:
                print("Start densification at sampling rate 1 out of ", round(resolution_scale ** 3))
                new_o3d_pcd = self.ref_o3d_pc.uniform_down_sample(round(resolution_scale ** 3))
            else:
                new_o3d_pcd = self.ref_o3d_pc

            new_number_points = np.asarray(new_o3d_pcd.points).shape[0]
            if propagate_position:
                old_point_cloud = torch.tensor(np.asarray(self.o3d_pc.points)).float()

            fused_point_cloud = torch.tensor(np.asarray(new_o3d_pcd.points)).float()
            fused_normals = torch.tensor(np.asarray(new_o3d_pcd.normals)).float()
            uncertainty = torch.zeros((new_number_points, 1), dtype=torch.float)
            print("New number of points: ", new_number_points)

            features = torch.zeros((new_number_points, self.global_features.shape[1]), dtype=torch.float)
            alpha = torch.zeros((new_number_points, 1), dtype=torch.float)
            print("Find the nearest neighbors and average optimisation parameters.")
            pcd_tree = o3d.geometry.KDTreeFlann(self.o3d_pc)
            print("KD Tree created")

            self.global_features = self.global_features.cpu()
            self.global_alpha = self.global_alpha.cpu()
            self.global_xyz = self.global_xyz.cpu()
            self.global_sigmoid_scale_u = self.global_sigmoid_scale_u.cpu()
            self.global_uncertainty = self.global_uncertainty.cpu()
            self.global_normals = self.global_normals.cpu()

            num_neighbors = 4  # 4 or 6
            for i in range(new_number_points):
                [_, idx, d] = pcd_tree.search_knn_vector_3d(new_o3d_pcd.points[i], num_neighbors)
                inv_d = 1.0 / (np.asarray(d) + 0.00001)
                sumd = np.sum(inv_d)
                for j in range(num_neighbors):
                    features[i, :] += torch.sigmoid(self.global_features[idx[j], :]) * inv_d[j] / sumd
                    alpha[i, :] += torch.sigmoid(self.global_alpha[idx[j], :]) * inv_d[j] / sumd

                    uncertainty[i, :] += self.global_sigmoid_scale_u[idx[j], :] * torch.sigmoid(
                        self.global_uncertainty[idx[j], :]) * inv_d[j] / sumd

                    if propagate_position:
                        fused_point_cloud[i, :] += (self.global_xyz[idx[j], :] - old_point_cloud[idx[j], :]) * inv_d[
                            j] / sumd
                        if normals:
                            fused_normals[i, :] += self.global_normals[idx[j], :] * inv_d[j] / sumd

            # dist2 = self.getDistances(new_o3d_pcd, 3)
            # dist2_clipped = np.clip(dist2, a_min=np.percentile(dist2, 0.01), a_max=np.percentile(dist2, 70))
            # sigmoid_scale = 4.0 * dist2_clipped
            # uncertainty = inverse_sigmoid_scaled(dist2_clipped, sigmoid_scale)
            # uncertainty = torch. tensor(uncertainty, dtype=torch.float, device="cuda").unsqueeze(-1)
            # self.global_sigmoid_scale_u = torch.tensor(sigmoid_scale, dtype=torch.float, device="cuda").unsqueeze(-1)

            uncertainty = 0.125 * uncertainty
            sigmoid_scale = 2.0 * uncertainty
            uncertainty = torch.log(uncertainty / (sigmoid_scale - uncertainty))
            self.global_sigmoid_scale_u = sigmoid_scale.float().cuda()

            print("Finished nearest neighbor blending")
            self.global_xyz = nn.Parameter(fused_point_cloud.cuda().requires_grad_(True))
            self.global_features = nn.Parameter(inverse_sigmoid(features).cuda().requires_grad_(True))
            self.global_uncertainty = nn.Parameter(uncertainty.cuda().requires_grad_(True))
            self.global_alpha = nn.Parameter(inverse_sigmoid(alpha).cuda().requires_grad_(True))
            if normals:
                fused_normals /= torch.sqrt(torch.sum((fused_normals ** 2), dim=1, keepdim=True))
                self.global_normals = nn.Parameter(fused_normals.cuda().requires_grad_(True))
            else:
                self.global_normals = nn.Parameter(torch.tensor([], device="cuda"))
            self.o3d_pc = new_o3d_pcd


    def initialize_random(self, mean, std, num_points, extra_features, uncertainty, initial_points=None):
        total_number_points = num_points
        if initial_points is None:
            initial_points=total_number_points

        self.global_xyz = nn.Parameter(torch.zeros((total_number_points, 3), device="cuda").requires_grad_(True))
        # self.global_normals = nn.Parameter(
        #     (10 ** 4) * torch.ones((total_number_points, 3), device="cuda").requires_grad_(True))
        self.global_normals = nn.Parameter(torch.tensor([], device="cuda"))

        self.global_features = nn.Parameter(
            inverse_sigmoid(0.5 * torch.ones((total_number_points, 3+extra_features), device="cuda")).requires_grad_(True))
        self.global_alpha = nn.Parameter(inverse_sigmoid(
            0.025 * torch.ones((total_number_points, 1), dtype=torch.float, device="cuda")).requires_grad_(True))
        self.global_uncertainty = nn.Parameter(
            torch.zeros((self.global_xyz.shape[0], 1), dtype=torch.float, device="cuda").requires_grad_(
                True))

        #x = np.random.normal(mean[0], std[0], size=initial_points)
        #y = np.random.normal(mean[1], std[1], size=initial_points)
        #z = np.random.normal(mean[2], std[2], size=initial_points)
        x = np.random.uniform(-std[0], std[0], size=initial_points) + mean[0]
        y = np.random.uniform(-std[1], std[1], size=initial_points) + mean[1]
        z = np.random.uniform(-std[2], std[2], size=initial_points) + mean[2]

        fused_point_cloud = torch.tensor(np.stack((x, y, z), axis=-1), dtype=torch.float, device="cuda")
        features = torch.zeros(fused_point_cloud.shape[0], 3 + extra_features)
        uncertainty = torch.tensor(uncertainty, dtype=torch.float, device="cuda")
        alpha = inverse_sigmoid(0.2 * torch.ones((fused_point_cloud.shape[0], 1), dtype=torch.float, device="cuda"))

        with torch.no_grad():
            self.global_xyz[:fused_point_cloud.shape[0]] = fused_point_cloud
            self.global_features[:fused_point_cloud.shape[0]] = features
            self.global_uncertainty[:fused_point_cloud.shape[0]] = uncertainty
            self.global_alpha[:fused_point_cloud.shape[0]] = alpha
            # if fused_normals is not None:
            #     self.global_normals[:fused_point_cloud.shape[0]] = fused_normals
            # else:
            #     self.global_normals = None

    def save_ply(self, path, shuffle=False):
        mkdir_p(os.path.dirname(path))

        xyz = self.global_xyz.detach().cpu().numpy()
        normals = self.global_normals.detach().cpu().numpy()
        rgb = torch.sigmoid(self.global_features[:,:3]).detach().cpu().numpy()
        extra_f = torch.sigmoid(self.global_features[:, 3:]).detach().cpu().numpy()
        alpha = torch.sigmoid(self.global_alpha).detach().cpu().numpy()
        extra_f = np.concatenate((extra_f, np.ones((extra_f.shape[0], 4 - extra_f.shape[1]))), axis=1)
        uncertainty = self.global_uncertainty.detach().cpu().numpy()
        u_scale = self.global_sigmoid_scale_u.detach().cpu().numpy()

        elements = np.empty(xyz.shape[0],
                            dtype=[('x', 'f4'), ('y', 'f4'), ('z', 'f4'),
                                   ('nx', 'f4'), ('ny', 'f4'), ('nz', 'f4'),
                                   ('red', 'f4'), ('green', 'f4'), ('blue', 'f4'),
                                   ('extra_0', 'f4'), ('extra_1', 'f4'), ('extra_2', 'f4'),
                                   ('specularity', 'f4'),
                                   ('alpha', 'f4'),
                                   ('uncertainty', 'f4'),
                                   ('u_scale', 'f4')])

        attributes = np.concatenate((xyz,
                                     normals,
                                     rgb,
                                     extra_f,
                                     alpha,
                                     uncertainty,
                                     u_scale), axis=1)
        if shuffle:
            rng = np.random.default_rng()
            rng.shuffle(attributes, axis=0)
        elements[:] = list(map(tuple, attributes))
        el = PlyElement.describe(elements, 'vertex')

        PlyData([el]).write(path)

    def reset_uncertainties(self):
        start_t = time.time()
        pcd = o3d.geometry.PointCloud()
        pcd.points = o3d.utility.Vector3dVector(self.global_xyz.detach().cpu().numpy())
        dist2 = self.getDistances(pcd, 3)
        dist2_clipped = np.clip(dist2,
                                a_min=0.00000001,
                                a_max=np.percentile(dist2, 90))
        sigmoid_scale = 4.0 * dist2_clipped
        uncertainty = inverse_sigmoid_scaled(dist2_clipped, sigmoid_scale)
        start = 0
        for idx in range(len(self._global_uncertainty)):
            size = self._global_uncertainty[idx].shape[0]
            self._global_uncertainty[idx] = torch.tensor(uncertainty[start:start+size], dtype=torch.float, device="cuda").unsqueeze(-1)
            self._global_sigmoid_scale_u[idx] = torch.tensor(sigmoid_scale[start:start+size], dtype=torch.float, device="cuda").unsqueeze(-1)
            start += size
        stop_t = time.time()
        print("Resetting U took: {:.2f}s".format(stop_t - start_t))

    def load_ply(self, path, active_normals=True, recompute_uncertainty=False):
        plydata = PlyData.read(path)

        xyz = np.stack((np.asarray(plydata.elements[0]["x"]),
                        np.asarray(plydata.elements[0]["y"]),
                        np.asarray(plydata.elements[0]["z"])),  axis=1)
        normals = np.stack((np.asarray(plydata.elements[0]["nx"]),
                            np.asarray(plydata.elements[0]["ny"]),
                            np.asarray(plydata.elements[0]["nz"])), axis=1)
        features = np.stack((np.asarray(plydata.elements[0]["red"]),
                             np.asarray(plydata.elements[0]["green"]),
                             np.asarray(plydata.elements[0]["blue"]),
                             np.asarray(plydata.elements[0]["extra_0"]),
                             np.asarray(plydata.elements[0]["extra_1"]),
                             np.asarray(plydata.elements[0]["extra_2"]),
                             np.asarray(plydata.elements[0]["specularity"])
                             ), axis=1)
        alpha = np.asarray(plydata.elements[0]["alpha"])[..., np.newaxis]

        nan_xyz_filter = ~np.isnan(xyz).any(axis=1)

        xyz = xyz[nan_xyz_filter]
        normals = normals[nan_xyz_filter]
        features = features[nan_xyz_filter]
        alpha = alpha[nan_xyz_filter]

        if recompute_uncertainty:
            pcd = o3d.geometry.PointCloud()
            pcd.points = o3d.utility.Vector3dVector(xyz)
            dist2 = self.getDistances(pcd, 3)
            dist2_clipped = np.clip(dist2,
                                    a_min=0.00000001,
                                    a_max=np.percentile(dist2, 90))
            sigmoid_scale = 4.0*dist2_clipped
            uncertainty = inverse_sigmoid_scaled(dist2_clipped, sigmoid_scale)
            sigmoid_scale = sigmoid_scale[..., np.newaxis]
            uncertainty = uncertainty[..., np.newaxis]

        else:
            uncertainty = np.asarray(plydata.elements[0]["uncertainty"])[..., np.newaxis]
            sigmoid_scale = np.asarray(plydata.elements[0]["u_scale"])[..., np.newaxis]

        uncertainty = torch.tensor(uncertainty, dtype=torch.float, device="cuda")

        self._global_xyz = [nn.Parameter(torch.tensor(xyz, device="cuda").requires_grad_(True))]
        self._global_normals = [nn.Parameter(torch.tensor(normals, device="cuda").requires_grad_(True))]
        self._global_features = [nn.Parameter(inverse_sigmoid(torch.tensor(features[..., :3+self.extra_features], device="cuda")).requires_grad_(True))]
        self._global_alpha = [nn.Parameter(inverse_sigmoid(torch.tensor(alpha, device="cuda")).requires_grad_(True))]
        self._global_uncertainty = [uncertainty]
        self._global_sigmoid_scale_u = [torch.tensor(sigmoid_scale, dtype=torch.float, device="cuda")]

        self.active_normals = active_normals

        self.optimizer = torch.optim.Adam([
            {'params': self._global_xyz, 'lr': self.lr_xyz},
            {'params': self._global_uncertainty, 'lr': self.lr_u},
            {'params': self._global_features, 'lr': self.lr_feat},
            {'params': self._global_alpha, 'lr': self.lr_alpha},
            {'params': self._global_normals, 'lr': self.lr_normal}
        ], lr=0.0, eps=1e-15)


    def densify_cgal(self, center_bias, radius, target_points, scratch_path, shuffle=False):
        print("Densifying center_bias: {} radius: {} target_points: {} current_points {}".format(center_bias, radius, target_points,
                                                                                                 self.global_features.shape[0]))
        start = time.time()
        with torch.no_grad():
            mkdir_p(scratch_path)
            exepath = cgal_executable_path
            path_current = os.path.join(scratch_path, "densify_input.ply")
            path_upsampled = os.path.join(scratch_path, "densify_output.ply")
            target_points = target_points
            self.save_ply(path_current, shuffle)
            subprocess.call([exepath, path_current, path_upsampled, str(target_points), str(center_bias), str(radius)])
            self.load_ply(path_upsampled, recompute_uncertainty=True)
        end = time.time()
        print("Densifiying took: {:.2f}s Points Now: {}".format(end - start, self.global_features.shape[0]))

    def densify_gradient(self, grads, grad_threshold, pos_only=True):
        print(f"Densifying point cloud with gradient threshold: {grad_threshold} position only: {pos_only}")

        start = time.time()
        # NOTE: gradients will be lost (just like in densify cgal)
        n_init_points = self.global_xyz.shape[0]
        # Extract points that satisfy the gradient condition
        selected_pts_mask = torch.where(torch.norm(grads, dim=-1) >= grad_threshold, True, False)
        with torch.no_grad():
            new_global_xyz = self.global_xyz[selected_pts_mask]
            new_global_normals = self.global_normals[selected_pts_mask]
            new_global_features = self.global_features[selected_pts_mask]
            new_global_uncertainty = self.global_uncertainty[selected_pts_mask]
            new_global_alpha = self.global_alpha[selected_pts_mask]
            new_global_sigmoid_scale_u = self.global_sigmoid_scale_u[selected_pts_mask]
            # Add small displacement to the new sampled points
            # TODO: tune this parameter
            #new_global_xyz += 0.01*torch.randn_like(new_global_xyz)
            self._global_xyz.append(nn.Parameter(new_global_xyz.requires_grad_(True)))
            self._global_normals.append(nn.Parameter(new_global_normals.requires_grad_(True)))
            self._global_features.append(nn.Parameter(new_global_features.requires_grad_(True)))
            self._global_uncertainty.append(nn.Parameter(new_global_uncertainty.requires_grad_(True)))
            self._global_alpha.append(nn.Parameter(new_global_alpha.requires_grad_(True)))
            self._global_sigmoid_scale_u.append(new_global_sigmoid_scale_u)

            self.optimizer.add_param_group({'params': self._global_xyz[-1], 'lr': self.lr_xyz, 'eps': 1e-15})
            self.optimizer.add_param_group({'params': self._global_normals[-1], 'lr': self.lr_normal, 'eps': 1e-15})
            self.optimizer.add_param_group({'params': self._global_features[-1], 'lr': self.lr_feat, 'eps': 1e-15})
            self.optimizer.add_param_group({'params': self._global_uncertainty[-1], 'lr': self.lr_u, 'eps': 1e-15})
            self.optimizer.add_param_group({'params': self._global_alpha[-1], 'lr': self.lr_alpha, 'eps': 1e-15})

        self.gradient_accum = torch.zeros((self.global_xyz.shape[0], 1), device="cuda")
        self.denom = torch.zeros((self.global_xyz.shape[0], 1), device="cuda")

        end = time.time()
        print(f"Densifiying took: {end-start:.2f}s New Points: {self.global_features.shape[0]} against before: {n_init_points}")

    def get_bounding_sphere(self):
        center = self.global_xyz.mean(dim=0)
        radius = torch.abs(center - self.global_xyz).max()
        return center, radius

    def debugView1(self, grads):
        import polyscope as ps

        ps.init()
        pc_og = ps.register_point_cloud("point_cloud_original",
                                        self.global_xyz.detach().cpu().numpy(),
                                        enabled=True, radius=0.00007)
        pc_og.add_scalar_quantity("og_colors",
                                  np.array(self.gradient_accum.squeeze().detach().cpu().numpy()),
                                  enabled=True, cmap='viridis')
        ps.show()

    def debugViewDensify(self):
        import polyscope as ps

        ps.init()
        pc_og = ps.register_point_cloud("point_cloud_original",
                                        self._global_xyz[0].detach().cpu().numpy(),
                                        enabled=True, radius=0.00007)
        pc_og.add_color_quantity("og_colors",
                                 np.repeat(np.array([[0.0, 0.0, 1.0]]), repeats=self._global_xyz[0].shape[0], axis=0),
                                 enabled=True)
        pc_densified = ps.register_point_cloud("point_cloud_densified", torch.cat(self._global_xyz[1:], dim=0).detach().cpu().numpy(),
                                               enabled=True, radius=0.00007)
        pc_densified.add_color_quantity("densified_colors",
                                        np.repeat(np.array([[1.0, 0.0, 0.0]]), repeats=torch.cat(self._global_xyz[1:], dim=0).shape[0], axis=0),
                                        enabled=True)
        ps.show()


    def debugView(self):
        import polyscope as ps

        ps.init()

        center, radius = self.get_bounding_sphere()

        o3d_sphere = o3d.geometry.TriangleMesh.create_sphere(radius=radius, resolution=100)
        ps_mesh = ps.register_surface_mesh("inner_sphere", np.asarray(o3d_sphere.vertices), np.asarray(o3d_sphere.triangles))

        o3d_sphere = o3d.geometry.TriangleMesh.create_sphere(radius=2*radius, resolution=100)
        ps_mesh = ps.register_surface_mesh("outer_sphere", np.asarray(o3d_sphere.vertices), np.asarray(o3d_sphere.triangles))


        ps_pc_diffuse = ps.register_point_cloud("point_cloud_diffuse", self.global_xyz.detach().cpu().numpy(), enabled=True, radius=0.00007)
        ps_pc_diffuse.add_color_quantity("point_cloud_colors", self.global_features[:,:3].detach().cpu().numpy(), enabled=True)


        theta = 2*math.pi*np.random.uniform(size=(1000000, 1))
        phi = np.arccos(1 - 2*np.random.uniform(size=(1000000, 1)))
        #r = np.random.uniform(low=radius.item(), high=2*radius.item(), size=(1000000, 1))
        #r = 2*radius.item()*np.abs(np.random.normal(scale=1.0, size=(1000000, 1))) + radius.item()
        r = radius.item()*np.ones((1000000, 1))

        xyz = np.concatenate((r * np.sin(phi) * np.cos(theta),
                              r * np.sin(phi) * np.sin(theta),
                              r * np.cos(phi)), axis=1)

        random_pc = ps.register_point_cloud("random", xyz, enabled=True,
                                            radius=0.00007)
        ps.show()
